<?php

namespace App\Services;

use Illuminate\Container\Container;
use Illuminate\Filesystem\Filesystem;
use Illuminate\Translation\FileLoader;
use Illuminate\Translation\Translator;
use Illuminate\Validation\Factory;

class ValidatorService
{
    private $validation;

    public function __construct(public string $language)
    {
        dd(base_path());
        $loader           = new FileLoader(new Filesystem(), \Str::afterLast(base_path(), 'public_html/') . '/lang');
        $translator       = new Translator($loader, $language);
        $this->validation = new Factory($translator, new Container());
    }

    public function validate(array $data, array $rules)
    {
        return $this->validation->make($data, $rules)->errors();
    }
}
