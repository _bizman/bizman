<?php

namespace App\Exceptions;

use Exception;

class FailedToCreateNewClientException extends Exception
{
}
