<?php

namespace App\Exceptions;

use Exception;

class FailedToSaveCalculateDataException extends Exception
{
}
