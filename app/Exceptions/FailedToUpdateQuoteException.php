<?php

namespace App\Exceptions;

use Exception;

class FailedToUpdateQuoteException extends Exception
{
}
