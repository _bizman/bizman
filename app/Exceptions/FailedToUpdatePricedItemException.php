<?php

namespace App\Exceptions;

use Exception;

class FailedToUpdatePricedItemException extends Exception
{
}
