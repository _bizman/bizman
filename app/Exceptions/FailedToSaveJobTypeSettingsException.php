<?php

namespace App\Exceptions;

use Exception;

class FailedToSaveJobTypeSettingsException extends Exception
{
}
