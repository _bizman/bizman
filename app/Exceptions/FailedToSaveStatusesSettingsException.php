<?php

namespace App\Exceptions;

use Exception;

class FailedToSaveStatusesSettingsException extends Exception
{
}
