<?php

namespace App\Exceptions;

use Exception;

class FailedToCreateNewPricedItemException extends Exception
{
}
