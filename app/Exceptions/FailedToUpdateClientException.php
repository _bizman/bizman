<?php

namespace App\Exceptions;

use Exception;

class FailedToUpdateClientException extends Exception
{
}
