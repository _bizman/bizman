<?php

namespace App\Models;

use App\Models\PricedItem;
use Database\Factories\JobTypeFactory;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class JobType extends Model
{
    use HasFactory;

    protected $guarded = ['id'];

    protected $fillable = ['user_id', 'type', 'abbreviation'];

    protected static function newFactory()
    {
        return JobTypeFactory::new();
    }

    public function pricedItems()
    {
        return $this->hasMany(PricedItem::class, 'job_type_id');
    }

    public function quoteElements()
    {
        return $this->hasMany(QuoteElement::class, 'job_type_id');
    }
}
