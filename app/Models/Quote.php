<?php

namespace App\Models;

use App\Models\Client;
use App\Models\JobType;
use Database\Factories\QuoteFactory;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Quote extends Model
{
    use HasFactory;

    protected $guarded = ['id'];
    protected $fillable = [
        'client_id',
        'user_id',
        'state_id',
        'name',
        'calculate',
        'notes'
    ];
    protected static function newFactory()
    {
        return QuoteFactory::new();
    }

    public function client()
    {
        return $this->belongsTo(Client::class);
    }

    public function state()
    {
        return $this->belongsTo(QuoteState::class);
    }

    public function pricedItems()
    {
        return $this->hasMany(QuoteElement::class);
    }

    public function getHoursSummaryAttribute()
    {
        $items = $this->pricedItems->map(function ($element) {
            return [
            'hours' => (int)$element->work_hours,
            'job' => $element->jobType->type,
            'job_id' => $element->jobType->id,
            ];
        })->toArray();

        $list = [];

        if ($this->calculate != null) {
            $calc = unserialize($this->calculate);

            foreach ($items as $index => $item) {
                $slug = str_slug($item['job'], '_');
                $list[$slug] = [
                    'job' => $item['job'],
                    'hours' => $item['hours'] + ($list[$slug]['hours'] ?? 0),
                ];
            }

            foreach ($list as $slug => $item) {
                foreach ($calc as $data) {
                    $job_type = JobType::find($data['job_id']);

                    if ($job_type) {
                        $dataSlug = str_slug($job_type->type, '_');

                        if (array_key_exists($dataSlug, $list)) {
                            $list[$dataSlug]['per_hour'] = $data['perhour'];
                            $list[$dataSlug]['negative'] = $data['negative'];
                            $list[$dataSlug]['positive'] = $data['positive'];
                        }
                    } else {
                        unset($list[$slug]);
                    }
                }
            }
        } else {
            foreach ($items as $index => $item) {
                $slug = str_slug($item['job'], '_');

                $list[$slug] = [
                    'job' => $item['job'],
                    'hours' => $item['hours'] + ($list[$slug]['hours'] ?? 0),
                    'per_hour' => 200,
                    'negative' => -10,
                    'positive' => 10,
                ];
            }
        }

        return $list;
    }

    public function getHoursAllAttribute()
    {
        return $this->pricedItems->map(function ($element) {
            return $element->work_hours;
        })->sum();
    }

    public function scopeSearch($query, array $filters)
    {
        if ($filters['search'] ?? false) {
            $query->orderBy('updated_at')->whereHas('client', function ($query) use ($filters) {
                $query->where('first_name', 'like', '%' . request('search') . '%')
                    ->orWhere('last_name', 'like', '%' . request('search') . '%')
                    ->orWhere('email', 'like', '%' . request('search') . '%')
                    ->orWhere('name', 'like', '%' . request('search') . '%')
                    ->orWhere('company', 'like', '%' . request('search') . '%')
                    ->orWhere('city', 'like', '%' . request('search') . '%');
            })->orWhere('name', 'like', '%' . request('search') . '%');
        }
    }
}
