<?php

namespace App\Models;

use Database\Factories\QuoteElementFactory;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class QuoteElement extends Model
{
    use HasFactory;

    protected $guarded = ['id'];

    protected $fillable = [
        'user_id',
        'quote_id',
        'job_type_id',
        'title',
        'content',
        'work_hours'
    ];

    protected static function newFactory()
    {
        return QuoteElementFactory::new();
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function quote()
    {
        return $this->belongsTo(Quote::class);
    }


    public function jobType()
    {
        return $this->belongsTo(JobType::class);
    }
}
