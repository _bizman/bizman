<?php

namespace App\Http\Controllers;

use App\Exceptions\FailedToCreateNewPricedItemException;
use App\Exceptions\FailedToUpdatePricedItemException;
use App\Models\JobType;
use App\Models\PricedItem;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;

class PricedItemController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $items =  PricedItem::latest('updated_at')->search(request(['search']))->where('user_id', Auth::user()->id)->paginate(15);
        return view('priceditems.index', compact('items'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('priceditems.create', [
            'types' => JobType::all()->where('user_id', Auth::user()->id)
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $rules = [
            'title' => 'required',
            'content' => 'required',
            'work_hours' => 'required',
            'job_type_id' => 'required',
        ];

        $validator = Validator::make($request->all(), $rules);

        if ($validator->fails()) {
            return redirect('/priceditems/create')
                ->withErrors($validator)
                ->withInput();
        }

        try {
            PricedItem::create(array_merge($validator->validated(), ['user_id' => Auth::user()->id]));
        } catch(FailedToCreateNewPricedItemException $errors) {
        }

        return redirect('/priceditems')->with('success', 'Elemnt dodany');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\PricedItem  $pricedItem
     * @return \Illuminate\Http\Response
     */
    public function show(PricedItem $pricedItem)
    {
        return view('priceditems.show', [
            'pricedItem' => $pricedItem,
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\PricedItem  $pricedItem
     * @return \Illuminate\Http\Response
     */
    public function edit(PricedItem $pricedItem)
    {
        return view('priceditems.edit', [
            'pricedItem' => $pricedItem,
            'types' => JobType::all()->where('user_id', Auth::user()->id)

        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\PricedItem  $pricedItem
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, PricedItem $pricedItem)
    {
        $rules = [
            'title' => 'required',
            'content' => 'required',
            'work_hours' => 'required',
            'job_type_id' => 'required',
        ];
        $validator = Validator::make($request->all(), $rules);

        if ($validator->fails()) {
            return redirect('/priceditems/' . $pricedItem->id)
                ->withErrors($validator)
                ->withInput();
        }
        try {
            $pricedItem->update($validator->validated());
        } catch(FailedToUpdatePricedItemException $errors) {
        }

        return redirect('/priceditems/' . $pricedItem->id)->with('success', 'Element zaktualizowany');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\PricedItem  $pricedItem
     * @return \Illuminate\Http\Response
     */
    public function destroy(PricedItem $pricedItem)
    {
        $pricedItem->delete();
        return back()->with('success', 'Element ' . $pricedItem->title  . ' został usunięty');
    }
}
