<?php

namespace App\Http\Controllers;

use App\Exceptions\FailedToCreateNewClientException;
use App\Exceptions\FailedToUpdateClientException;
use App\Models\Client;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;

class ClientController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $clients =  Client::latest('updated_at')
            ->search(request(['search']))
            ->where('user_id', Auth::user()->id)
            ->paginate(15);

        return view('clients.index', compact('clients'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('clients.create', []);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $rules = [
            'first_name' => 'required',
            'last_name' => 'required',
            'phone_number' => 'nullable|numeric',
            'email' => 'nullable|unique:clients|email:rfc,dns',
            'notes' => 'nullable|string',

            'company' => 'nullable|max:255',
            'tax_id' => 'nullable|max:255',
            'company_id' => 'nullable|max:255',
            'website' => 'nullable|max:255',

            'street' => 'nullable|max:255',
            'house' => 'nullable|max:255',
            'house_number' => 'nullable|max:255',
            'city' => 'nullable|max:255',
            'postal' => 'nullable|max:255',
            'country' => 'nullable|max:255',

            'voivodeship' => 'nullable|max:255',
            'county' => 'nullable|max:255',
            'borough' => 'nullable|max:255',
        ];

        $validator = Validator::make($request->all(), $rules);

        if ($validator->fails()) {
            return redirect('/clients/create')
                ->withErrors($validator)
                ->withInput();
        }

        try {
            Client::create(array_merge($validator->validated(), ['user_id' => Auth::user()->id]));
        } catch(FailedToCreateNewClientException $errors) {
        }

        return redirect('/clients');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Client  $Client
     * @return \Illuminate\Http\Response
     */
    public function show(Client $client)
    {
        return view('clients.show', [
            'client' => $client,
            'quotes' => $client->quotes
        ]);
    }
    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Quote  $quote
     * @return \Illuminate\Http\Response
     */
    public function edit(Client $client)
    {
        return view('clients.edit', [
            'client' => $client
        ]);
    }
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Client  $client
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Client $client)
    {
        $rules = [
            'first_name' => 'required',
            'last_name' => 'required',
            'phone_number' => '',
            'email' => [Rule::unique('clients', 'email')->ignore($client->id), 'email:rfc,dns', 'nullable'],
            'notes' => 'nullable|string',

            'company' => 'nullable|max:255',
            'tax_id' => 'nullable|max:255',
            'company_id' => 'nullable|max:255',
            'website' => 'nullable|max:255',

            'street' => 'nullable|max:255',
            'house' => 'nullable|max:255',
            'house_number' => 'nullable|max:255',
            'city' => 'nullable|max:255',
            'postal' => 'nullable|max:255',
            'country' => 'nullable|max:255',

            'voivodeship' => 'nullable|string|max:255',
            'county' => 'nullable|string|max:255',
            'borough' => 'nullable|string|max:255',
        ];

        $validator = Validator::make($request->all(), $rules);

        if ($validator->fails()) {
            return redirect('/clients/create')
                ->withErrors($validator)
                ->withInput();
        }

        try {
            $client->update($validator->validated());
        } catch(FailedToUpdateClientException $errors) {
        }


        return redirect('/clients/' . $client->id)->with('success', 'Klient zaktualizowany');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Client  $client
     * @return \Illuminate\Http\Response
     */
    public function destroy(Client $client)
    {
        if ($client->quotes()->count() == 0) {
            $client->delete();
            return back()->with('success', 'Klient ' . $client->first_name . ' ' . $client->last_name . ' został usunięty');
        } else {
            return back()->with('errorMessage', 'Nie można usunąć klienta, który posiada przypisane wyceny');
        }
    }
}
