<?php

namespace App\Http\Controllers;

use App\Exceptions\FailedToCreateNewQuoteException;
use App\Exceptions\FailedToSaveCalculateDataException;
use App\Exceptions\FailedToUpdateQuoteException;
use App\Models\Client;
use App\Models\JobType;
use App\Models\PricedItem;
use App\Models\Quote;
use App\Models\QuoteElement;
use App\Models\QuoteState;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;

class QuoteController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $clients = Client::where('user_id', Auth::user()->id)->count();

        $quotes =  Quote::latest('updated_at')
            ->search(request(['search']))
            ->where('user_id', Auth::user()->id)
            ->paginate(15);

        return view('quotes.index', compact('quotes', 'clients'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $user_clients = Client::all()->where('user_id', Auth::user()->id);
        $quote_states = QuoteState::all()->where('user_id', Auth::user()->id);
        $priced_items = PricedItem::all()->where('user_id', Auth::user()->id);
        $job_types = JobType::all()->where('user_id', Auth::user()->id);

        return view('quotes.create', [
            'clients' => $user_clients,
            'states' => $quote_states,
            'priced_items' => $priced_items,
            'job_types' => $job_types
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $rules = [
            'client_id' => 'required',
            'state_id' => 'required',
            'name' => 'required',

            'quote_items.*.title' => 'required|string',
            'quote_items.*.content' => 'sometimes|required|string',
            'quote_items.*.job_type_id' => 'required|numeric',
            'quote_items.*.work_hours' => 'required|numeric',
            'quote_items.*.add_to_priced' => 'sometimes|accepted',

            'notes' => 'nullable|string',
        ];

        $validator = Validator::make($request->all(), $rules);

        if ($validator->fails()) {
            return redirect('/quotes/create')
                ->withErrors($validator)
                ->withInput();
        }

        try {
            $quote = Quote::create([
                'user_id' => Auth::user()->id,
                'client_id' => $request->get('client_id'),
                'state_id' =>  $request->get('state_id'),
                'name' =>  $request->get('name'),
                'notes' =>  $request->get('notes')
            ]);

            if ($request->hasAny('quote_items')) {
                collect($request->get('quote_items'))->each(function ($item) use ($quote) {
                    QuoteElement::create([
                        'user_id' => Auth::user()->id,
                        'quote_id' => $quote->id,
                        'job_type_id' => $item['job_type_id'],
                        'title' => $item['title'],
                        'content' => $item['content'],
                        'work_hours' => $item['work_hours']
                    ]);

                    if (array_key_exists('add_to_priced', $item)) {
                        PricedItem::create([
                        'job_type_id' => $item['job_type_id'],
                        'title' => $item['title'],
                        'content' => $item['content'],
                        'work_hours' => $item['work_hours'],
                        'user_id' => Auth::user()->id,
                        ]);
                    }
                });
            }
        } catch(FailedToCreateNewQuoteException $errors) {
        }

        return redirect('/quotes/'.$quote->id);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Quote  $quote
     * @return \Illuminate\Http\Response
     */
    public function show(Quote $quote)
    {
        $jobs = collect(JobType::all()->where('user_id', Auth::user()->id))->map(function ($job) use ($quote) {
            $sum = collect($quote->pricedItems)->map(function ($item) use ($job) {
                if ($item->job_type_id == $job->id) {
                    return $item->work_hours;
                }
            })->sum();

            return [
                'abbr' => $job->abbreviation,
                'type' => $job->type,
                'id' => $job->id,
                'time' => $sum
            ];
        })->toArray();

        return view('quotes.show', [
            'quote' => $quote,
            'jobs' => $jobs,
            'calculate' => $quote->calculate ? unserialize($quote->calculate) : null
        ]);
    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Quote  $quote
     * @return \Illuminate\Http\Response
     */
    public function edit(Quote $quote)
    {
        $priced_items = PricedItem::all()->where('user_id', Auth::user()->id);
        $job_types = JobType::all()->where('user_id', Auth::user()->id);
        $user_clients = Client::all()->where('user_id', Auth::user()->id);
        $quote_states = QuoteState::all()->where('user_id', Auth::user()->id);
        $job_types = JobType::all()->where('user_id', Auth::user()->id);

        return view('quotes.edit', [
            'quote' => $quote,
            'clients' => $user_clients,
            'states' => $quote_states,
            'selected' => $quote->pricedItems,
            'priced_items' => $priced_items,
            'job_types' => $job_types
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Quote  $quote
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Quote $quote)
    {
        $rules = [
            'client_id' => 'required',
            'state_id' => 'required',
            'name' => 'required',

            'quote_items.*.title' => 'required|string',
            'quote_items.*.content' => 'sometimes|required|string',
            'quote_items.*.job_type_id' => 'required|numeric',
            'quote_items.*.work_hours' => 'required|numeric',
            'quote_items.*.add_to_priced' => 'sometimes|accepted',
            'quote_items.*.remove' => 'sometimes|accepted',
            'quote_items.*.item_id' => 'sometimes|required|numeric',

            'notes' => 'nullable|string',
        ];

        $validator = Validator::make($request->all(), $rules);

        if ($validator->fails()) {
            return redirect('/quotes/'.$quote->id.'/edit')
                ->withErrors($validator)
                ->withInput();
        }

        try {
            $quote->update([
                'client_id' => $request->get('client_id'),
                'state_id' =>  $request->get('state_id'),
                'name' =>  $request->get('name'),
                'notes' =>  $request->get('notes')
            ]);

            if ($request->hasAny('quote_items')) {
                collect($request->get('quote_items'))->each(function ($item) use ($quote) {
                    if (array_key_exists('item_id', $item)) { //update or delete
                        $quote_item = QuoteElement::find($item['item_id']);

                        if ($quote_item) { //exists
                            if (array_key_exists('remove', $item)) { //remove
                                $quote_item->delete();
                            } else { //update
                                $quote_item->update([
                                    'job_type_id' => $item['job_type_id'],
                                    'title' => $item['title'],
                                    'content' => $item['content'],
                                    'work_hours' => $item['work_hours'],
                                ]);
                            }
                        }
                    } else { //create
                        QuoteElement::create([
                               'user_id' => Auth::user()->id,
                               'quote_id' => $quote->id,
                               'job_type_id' => $item['job_type_id'],
                               'title' => $item['title'],
                               'content' => $item['content'],
                               'work_hours' => $item['work_hours']
                           ]);
                    }


                    if (array_key_exists('add_to_priced', $item)) {
                        PricedItem::create([
                            'job_type_id' => $item['job_type_id'],
                            'title' => $item['title'],
                            'content' => $item['content'],
                            'work_hours' => $item['work_hours'],
                            'user_id' => Auth::user()->id,
                        ]);
                    }
                });
            }
        } catch(FailedToUpdateQuoteException $errors) {
        }

        return redirect('/quotes/' . $quote->id)->with('success', 'Element zaktualizowany');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Quote  $quote
     * @return \Illuminate\Http\Response
     */
    public function destroy(Quote $quote)
    {
        if ($quote->pricedItems->count() == 0) {
            $quote->delete();
            return back()->with('success', 'Wycena ' . $quote->name  . ' została usunięta');
        } else {
            $quote->pricedItems->each(fn ($item) => $item->delete());
            $quote->delete();
            return back()->with('success', 'Wycena ' . $quote->name  . ' została usunięta');
        }
    }

    public function calculate(Request $request, Quote $quote)
    {
        $rules = ([
            'calculator.*.perhour' => 'required|numeric',
            'calculator.*.negative' => 'required|numeric',
            'calculator.*.positive' => 'required|numeric',
            'calculator.*.job_id' => 'required|numeric',
        ]);

        $validator = Validator::make($request->all(), $rules);

        if ($validator->fails()) {
            return redirect('/quotes/' . $quote->id)
                ->withErrors($validator)
                ->withInput();
        }

        try {
            $quote->update([
                'calculate' => serialize($request->calculator)
            ]);
        } catch(FailedToSaveCalculateDataException $errors) {
        }

        return redirect('/quotes/' . $quote->id)->with('success', 'Zmiany zapisane');
    }
}
