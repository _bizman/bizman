<?php

namespace App\Http\Controllers\Settings;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;

class UserDataController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('settings.user', []);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\UserSetting  $userSetting
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $user = Auth::user();

        $rules =  [
            'user_name' => [
                'required',
                'min:2'
            ],
            'user_email' => [
                'required',
                'email',
                'unique:users,email,' . $user->id
            ],
        ];

        $validator = Validator::make($request->all(), $rules);


        if ($validator->fails()) {
            return redirect('user/settings/user')
                ->withErrors($validator)
                ->withInput();
        }


        if ($request->user_email != $user->email) {
            $user->email = $request->user_email;
        }
        $user->name = $request->user_name;
        $user->save();


        return redirect('/user/settings/user')->with('success', 'Twoje dane zostały zaktualizowane');
    }
}
