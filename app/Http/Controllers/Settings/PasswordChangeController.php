<?php

namespace App\Http\Controllers\Settings;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rules\Password;

class PasswordChangeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('settings.password', []);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\UserSetting  $userSetting
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $rules =  [
            'current_password' => 'required',
            'user_password' => [
                'required',
                Password::min(8)
                    ->mixedCase()
                    ->numbers()
                    ->symbols()
                    ->uncompromised(),
                'confirmed'
            ],
        ];


        $validator = Validator::make($request->all(), $rules);


        if ($validator->fails()) {
            return redirect('user/settings/password')
                ->withErrors($validator)
                ->withInput();
        }

        $user = Auth::user();

        if (!Hash::check($request->current_password, $user->password)) {
            return redirect('user/settings/password')
                ->withErrors(['current_password' => 'Obecne hasło nie jest poprawne'])
                ->withInput();
        } else {
            $user->password = Hash::make($request->user_password);
            $user->save();
        }

        return redirect('/user/settings/password')->with('success', 'Hasło zostało zmienione');
    }
}
