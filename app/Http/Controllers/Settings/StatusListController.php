<?php

namespace App\Http\Controllers\Settings;

use App\Exceptions\FailedToSaveStatusesSettingsException;
use App\Http\Controllers\Controller;
use App\Models\QuoteState;
use Illuminate\Http\Request;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;

class StatusListController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $quote_statuses = QuoteState::all()->where('user_id', Auth::user()->id);
        return view('settings.statuses', compact('quote_statuses'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $rules =  [
            'statuses.*.state' => [
                'sometimes',
                'min:1',
                'required'
            ],
            'statuses.*.color' => [
                'sometimes',
                'regex:/^#([a-f0-9]{6}|[a-f0-9]{3})$/i',
                'required'
            ],
            'statuses.*.id' => [
                'sometimes',
                'required',
                'exists:quote_states,id'
            ],
            'statuses.*.delete' => [
                'sometimes',
                'accepted'
            ],
            'default_status' => [
                'sometimes',
                'required',
                'exists:quote_states,id'
            ]
        ];

        $validator = Validator::make($request->all(), $rules);

        if ($validator->fails()) {
            return redirect('user/settings/statuses')
                ->withErrors($validator)
                ->withInput();
        }

        try {
            collect($request->statuses)->filter(function ($item) { //create new
                $item = $item instanceof Collection ? $item : Collection::make($item);

                return !$item->has('id');
            })->map(function ($item) {
                $item = $item instanceof Collection ? $item : Collection::make($item);

                $quote_state = new QuoteState([
                    'user_id' => Auth::user()->id,
                    'state' => $item->get('state'),
                    'color' => $item->get('color')
                ]);

                $quote_state->save();
            });

            collect($request->statuses)->filter(function ($item) { //update or delete
                $item = $item instanceof Collection ? $item : Collection::make($item);

                return $item->has('id');
            })->map(function ($item) use ($request) {
                $item = $item instanceof Collection ? $item : Collection::make($item);

                $status = QuoteState::find($item->get('id'));

                if ($status->user_id == Auth::user()->id) {
                    //marked for deletion
                    if ($item->has('delete')) {
                        $quote_state = QuoteState::find($request->get('default_status'));

                        $status->quotes->each(function ($item) use ($quote_state) {
                            $item->state_id = $quote_state->id;
                            $item->save();
                        });

                        $status->delete();
                    }
                    //update
                    else {
                        $status->state = $item->get('state');
                        $status->color = $item->get('color');
                        $status->save();
                    }
                }
            });
        } catch (FailedToSaveStatusesSettingsException $exception) {
        }

        return redirect('/user/settings/statuses')->with('success', 'Ustawienia statusów zostały zaktualizowane');
    }
}
