<?php

namespace App\Http\Controllers\Settings;

use App\Exceptions\FailedToSaveJobTypeSettingsException;
use App\Http\Controllers\Controller;
use App\Models\JobType;
use App\Models\UserSetting;
use Illuminate\Http\Request;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;

class JobTypeListController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $job_types = JobType::all()->where('user_id', Auth::user()->id);

        return view('settings.jobs', compact('job_types'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\UserSetting  $userSetting
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, UserSetting $userSetting)
    {
        $rules =  [
            'jobtypes.*.type' => [
                'sometimes',
                'min:1',
                'required'
            ],
            'jobtypes.*.abbreviation' => [
                'sometimes',
                'min:1',
                'max:6',
                'required'
            ],
            'jobtypes.*.id' => [
                'sometimes',
                'required',
                'exists:job_types,id'
            ],
            'jobtypes.*.delete' => [
                'sometimes',
                'accepted'
            ],
            'default_job_type' => [
                'sometimes',
                'required',
                'exists:job_types,id'
            ]
        ];

        $validator = Validator::make($request->all(), $rules);

        if ($validator->fails()) {
            return redirect('user/settings/categories')
                ->withErrors($validator)
                ->withInput();
        }

        try {
            collect($request->jobtypes)->filter(function ($item) { //create new
                $item = $item instanceof Collection ? $item : Collection::make($item);

                return !$item->has('id');
            })->map(function ($item) {
                $item = $item instanceof Collection ? $item : Collection::make($item);

                $job = new JobType([
                    'user_id' => Auth::user()->id,
                    'type' => $item->get('type'),
                    'abbreviation' => $item->get('abbreviation')
                ]);

                $job->save();
            });

            collect($request->jobtypes)->filter(function ($item) use ($request) { //update or delete
                $item = $item instanceof Collection ? $item : Collection::make($item);

                return $item->has('id');
            })->map(function ($item) use ($request) {
                $item = $item instanceof Collection ? $item : Collection::make($item);

                $job = JobType::find($item->get('id'));

                if ($job->user_id == Auth::user()->id) {
                    //marked for deletion
                    if ($item->has('delete')) {
                        $job_type = JobType::find($request->get('default_job_type'));

                        $job->pricedItems->each(function ($item) use ($job_type) {
                            $item->job_type_id = $job_type->id;
                            $item->save();
                        });

                        $job->quoteElements->each(function ($item) use ($job_type) {
                            $item->job_type_id = $job_type->id;
                            $item->save();
                        });

                        $job->delete();
                    }
                    //update
                    else {
                        $job->type = $item->get('type');
                        $job->abbreviation = $item->get('abbreviation');
                        $job->save();
                    }
                }
            });
        } catch (FailedToSaveJobTypeSettingsException $exception) {
        }

        return redirect('/user/settings/categories')->with('success', 'Ustawienia kategorii zostały zaktualizowane');
    }
}
