<?php

namespace App\Providers;

use App\View\Composers\ProfileComposer;
use App\View\Composers\Settings\NavigationComposer;
use Illuminate\Support\Facades\View;
use Illuminate\Support\ServiceProvider;

class ViewServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        View::composer('settings.partials.navigation', NavigationComposer::class);
    }
}
