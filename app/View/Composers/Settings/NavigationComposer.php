<?php

namespace App\View\Composers\Settings;

use Illuminate\Http\Client\Request;
use Illuminate\Support\Facades\Route;
use Illuminate\View\View;

class NavigationComposer
{
    protected $tabs;

    protected $activeClasses;

    protected $tabClasses;

    public function __construct()
    {
        $this->tabs = [
            'settings_statuses' => 'Statusy',
            'settings_jobs' => 'Kategorie wycenionych elementów',
            'settings_user' => 'Ustawienia użytkownika',
            'settings_password' => 'Ustawienia hasła',
        ];
        $this->tabClasses = [
            'px-4', 'py-2', 'text-base', 'font-medium', 'text-black', 'transition-all', 'bg-white', 'border', 'border-gray-300', 'rounded-lg', 'hover:border-blue', 'hover:bg-blue', 'hover:text-white'
        ];
        $this->activeClasses = [
            'px-4', 'py-2', 'text-base', 'font-medium', 'text-white', 'transition-all', 'bg-blue', 'border', 'border-blue', 'rounded-lg', 'select-none'
        ];
    }

    public function activeTab(): string
    {
        return Route::currentRouteName();
    }

    public function compose(View $view)
    {
        $view->with([
            'tabs' => $this->tabs,
            'activeClass' => $this->activeTab(),
            'activeTab' => $this->activeTab(),
            'activeClasses' => implode(' ', $this->activeClasses),
            'tabClasses' => implode(' ', $this->tabClasses)
        ]);
    }
}
