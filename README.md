# KONFIGURACJA LOKALNEGO ŚRODOWISKA

Dla czystego ubuntu 22.04 LTS

## AKTUALIZACJA SYSTEMU
```
sudo apt update
sudo apt upgrade
```
## ODINSTALOWANIE APACHE2

```
sudo systemctl stop apache2
sudo apt-get --purge autoremove apache2
```

## INSTALACJA NGINX
```
sudo apt install nginx
```

## INSTALACJA PHP 8.1
```
sudo apt install php
sudo apt install php-{bz2,curl,intl,mysql,mbstring,readline,xml}
sudo apt install php-fpm
sudo apt install curl zip unzip php-zip
```


## INSTALACJA MYSQL (VER 8)
```
sudo apt install mysql-server
```

## DODANIE USERA DO BAZY (DODAJ SWOJEGO I ZMIEN WARTOSC W .ENV.EXAMPLE)
logowanie do mysql shell
```
mysql -u root
```
w konsoli mysql

```
CREATE USER 'bizman'@'localhost' IDENTIFIED BY 'haslo';
GRANT ALL PRIVILEGES ON *.* TO 'marek'@'localhost' WITH GRANT OPTION;
ALTER USER 'bizman'@'localhost' IDENTIFIED WITH mysql_native_password BY 'haslo';
FLUSH PRIVILEGES;
exit
```

## LOGOWANIE NA NOWYM USER
```
mysql -u bizman -p
```

## TWORZENIE BAZY 
```
CREATE DATABASE bizman;
```

## INSTALACJA COMPOSER (PACKAGE MANAGER PHP)
```
sudo apt install composer
```

## KLONOWANIE REPO
```
cd /var/www
git clone https://gitlab.com/_bizman/bizman.git
```
logowanie do gitlaba (kazdy ma swojego usera)

## INSTALACJA NVM NODE I NPM
```
curl -o- https://raw.githubusercontent.com/nvm-sh/nvm/v0.39.1/install.sh | bash
source ~/.bashrc
nvm list-remote
nvm install v18.12.1
```

## INSTALACJA APLIKACJI (PHP DEPENDENCIES)
```
composer install
```

## TWORZENIE PLIKU KONFIGURACJI .ENV
```
cp .env.example .env
```

## MIGRACJA TABEL W BAZIE DANYCH
```
php artisan migrate
```

## POBRANIE JS DEPENDENCIES PRZEZ NPM
```
npm install
npm run dev
```

## DODANIE PLIKU KONFIGURACJI NGINX
```
cp nginx.conf.sample /etc/nginx/sites-available/bizman
```

## SYMLINK NGINX
```
cd /etc/nginx/sites-enabled
sudo ln -s ../sites-available/bizman .
ls -l
```

## DODANIE WPISU DO PLIKU ETC/HOSTS (NP POD LOCALHOST)
```
nano /etc/hosts
```
127.0.0.1 bizman.local

## RESTART NGINX
```
systemctl restart nginx
```

## KONFIGURACJA DOSTEPOW DLA GRUPY NGINX
```
cd /var/www/html/bizman/
sudo chown -R $USER:www-data storage
sudo chown -R $USER:www-data bootstrap/cache
```

## ZMIANA UPRAWNIEN DLA KATALOGOW
```
chmod -R 775 storage
chmod -R 775 bootstrap/cache
```
