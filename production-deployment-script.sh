#!/bin/bash

php artisan down
git pull origin master
cp .env.prod .env
composer install --no-dev
php artisan migrate 
npm install
npm run prod
php artisan up