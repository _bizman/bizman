<?php

namespace Database\Seeders;

use App\Models\JobType;
use App\Models\PricedItem;
use App\Models\User;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class PricedItemSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $elements = [
            [
                'job_type_id' => 2,
                'title' => 'Wizerunkowa strona internetowa',
                'content' => 'Wizerunkowa strona internetowa zbudowana na bazie systemu zarządzania treścią (CMS WordPress) umożliwiająca samodzielne zarządzanie treściami na stronie internetowej.',
            ], [
                'job_type_id' => 2,
                'title' => 'Tylko polski',
                'content' => 'Strona wykonana w polskiej wersji językowej.',
            ], [
                'job_type_id' => 3,
                'title' => 'Wielojęzyczność',
                'content' => 'Obsługa wprowadzania treści w języku ',
            ], [
                'job_type_id' => 3,
                'title' => 'Zgodność z pozycjonowaniem',
                'content' => 'Strona ułatwiająca dotarcie do docelowej grupy Klientów dzięki zgodności z wytycznymi Google w aspekcie pozycjonowania.',
            ], [
                'job_type_id' => 0,
                'title' => 'Indywidualny projekt',
                'content' => 'Indywidualny, profesjonalny projekt szaty graficznej strony internetowej dopasowany do docelowej grupy Klientów spójny z systemem identyfikacji wizualnej marki.',
            ], [
                'job_type_id' => 3,
                'title' => 'RWD',
                'content' => 'Wdrożenie szaty graficznej wraz z animacjami do systemu CMS zgodnie z normami RWD (Responsive Web Design), tj. zgodności wyświetlania strony internetowej na urządzeniach mobilnych i komputerach desktop.',
            ], [
                'job_type_id' => 3,
                'title' => 'Blog',
                'content' => 'Blog – moduł aktualności<br> •Możliwość publikowania wpisów<br> •Możliwość ustalenia daty publikacji oraz terminu ważności wpisu<br> •Możliwość publikowania filmów z YouTube',
            ], [
                'job_type_id' => 3,
                'title' => 'Kariera',
                'content' => 'Kariera - moduł rekrutacyjny<br> •Przedstawienie benefitów<br> •Opinie pracowników<br> •Przedstawienie procesu rekrutacji<br> •Przekierowanie na strony pracuj.pl i LinkedIn<br> •Możliwość rozbudowy o dedykowany moduł integracyjny LinkedIn',
            ], [
                'job_type_id' => 3,
                'title' => 'Anonimowe zgłoszenie',
                'content' => 'Anonimowy moduł zgłaszania nieprawidłowości<br> •Dedykowany formularz zgłoszenia działań niepożądanych<br> •Publikacja plików np. formularze i klauzula informacyjna<br> •Indywidualny kod zgłoszenia w celu weryfikacji statusu i odpowiedzi<br> •Powiadomienia mailowe dla Administratora systemu',
            ], [
                'job_type_id' => 3,
                'title' => 'Przekierowanie',
                'content' => 'Przekierowanie do',
            ], [
                'job_type_id' => 3,
                'title' => 'Formularz kontaktowy',
                'content' => 'Formularz kontaktowy wraz ze zgodami RODO z możliwością wysłania pliku',
            ], [
                'job_type_id' => 3,
                'title' => 'Mapa Google',
                'content' => 'Integracja z Google Maps API i wdrożenie szaty graficznej mapy',
            ], [
                'job_type_id' => 3,
                'title' => 'Mapa OSM',
                'content' => 'Integracja z Open Street Maps API',
            ], [
                'job_type_id' => 3,
                'title' => 'Social media',
                'content' => 'Powiązanie z profilami w Social Mediach (Facebook, Instagram)',
            ], [
                'job_type_id' => 3,
                'title' => 'Optymalizacja',
                'content' => 'Optymalizacja szybkości strony internetowej pod kątem działania i zgodności z najnowszymi wytycznymi Google pozwalające na lepsze indeksowanie strony w organicznych wynikach wyszukiwania',
            ], [
                'job_type_id' => 3,
                'title' => 'Analityka',
                'content' => 'Integracja strony z usługami Google: Analytics, Search Console',
            ], [
                'job_type_id' => 3,
                'title' => 'Wprowadzanie treści',
                'content' => 'Przygotowanie treści, zdjęć oraz ich wprowadzenie po stronie Zamawiającego',
            ], [
                'job_type_id' => 2,
                'title' => 'Wprowadzanie treści',
                'content' => 'Przygotowanie treści, zdjęć oraz ich wprowadzenie po stronie osoby realizującej',
            ], [
                'job_type_id' => 3,
                'title' => 'Wsparcie techniczne',
                'content' => 'Wsparcie techniczne/gwarancja na stronę – 12 miesięcy od momentu wdrożenia',
            ],
            [
                'job_type_id' => 4,
                'title' => 'Logo',
                'content' => 'Przygotowanie profesjonalnego projektu logo w 4 propozycjach.',
            ],
            [
                'job_type_id' => 1,
                'title' => 'Wsparcie techniczne',
                'content' => 'Wsparcie techniczne/gwarancja na stronę – 12 miesięcy od momentu wdrożenia',
            ],
            [
                'job_type_id' => 4,
                'title' => 'Księga znaku',
                'content' => 'Projekt logo wraz z kompletną księgą znaku. Kompletny produkt, jakim jest graficzne opisanie marki wraz z określeniem wszystkich zasad stosowania logo – od określenia jego kształtu, wersji podstawowej, rozszerzonej czy też logotypu okolicznościowego.',
            ],
            [
                'job_type_id' => 4,
                'title' => 'Wizytówka',
                'content' => 'Projekt graficzny firmowej wizytówki gotowy do druku',
            ],
            [
                'job_type_id' => 3,
                'title' => '',
                'content' => '',
            ],
            [
                'job_type_id' => 3,
                'title' => 'Mapa inwestycji',
                'content' => 'Moduł aktywnej mapy inwestycji wykonanej w oparciu o dostarczone rzuty inwestycji zawierająca:
                <br> •Plan inwestycji  Dane z dostępnością poszczególnych budynków w formie listingu
                <br> •Zaznaczanie dostępności budynków z poziomu Panelu Administracyjnego CMS
                <br><br>Przedstawienie budynków/lokali w budynkach dwulokalowych na osobnych podstronach zawierających:
                <br> •Rzuty budynku/lokalu Wizualizacje
                <br> •Cenę (dostępna opcja ceny regularnej i promocyjnej)
                <br> •Status (dostępny/zarezerwowany/sprzedany)
                <br> •Opis
                <br> •Specyfikację materiałową
                <br> •Pokazanie inwestycji na mapie zbiorczej jako miniatura
                <br> •Możliwość publikacji rzutów i kart katalogowych w formacie PDF do pobrania przez Klienta
                <br> •Formularz kontaktowy (lead) zbierający informacje od potencjalnego Klienta (przekazywanie adresu strony z której został wysłany formularz jako danej linkowej w treści maila)
                <br> •Galeria wizualizacji/zdjęć',
            ],
            [
                'job_type_id' => 3,
                'title' => 'Wdrożenie kompletne',
                'content' => 'Wdrożenie treści na stronę internetową – kompletne, po dostarczeniu wszystkich materiałów przez Klienta.',
            ],
            [
                'job_type_id' => 3,
                'title' => 'Prace programistyczne - funkcjonalności',
                'content' => 'Prace programistyczne związane z przygotowaniem, wdrożeniem i adaptacją wymaganych funkcjonalności do oprogramowania systemu CMS.',
            ],
            [
                'job_type_id' => 3,
                'title' => 'Prace programistyczne - szata',
                'content' => 'Prace programistyczne związane z wdrożeniem szaty graficznej wraz z animacjami do systemu zgodnie z normami RWD (zgodność wyświetlania na urządzeniach mobilnych i komputerach desktop).',
            ],
            // [
            //     'job_type_id' => 3,
            //     'title' => 'Wizytówka',
            //     'content' => '',
            // ],
            // [
            //     'job_type_id' => 3,
            //     'title' => 'Wizytówka',
            //     'content' => '',
            // ],
            // [
            //     'job_type_id' => 3,
            //     'title' => 'Wizytówka',
            //     'content' => '',
            // ],
            // [
            //     'job_type_id' => 3,
            //     'title' => 'Wizytówka',
            //     'content' => '',
            // ],
            // [
            //     'job_type_id' => 3,
            //     'title' => 'Wizytówka',
            //     'content' => '',
            // ],
            // [
            //     'job_type_id' => 3,
            //     'title' => 'Wizytówka',
            //     'content' => '',
            // ],
            // [
            //     'job_type_id' => 3,
            //     'title' => 'Wizytówka',
            //     'content' => '',
            // ]


        ];

        $users = User::all();

        foreach ($users as $user) {
            foreach ($elements as $key => $element) {
                if ($key <= 20) {
                    $jobType = JobType::all()->where('user_id', $user->id)->random();

                    DB::table('priced_items')->insert([
                        'user_id' => $user->id,
                        'job_type_id' =>  $jobType->id,
                        'title' => $element['title'],
                        'content' => $element['content'],
                        'work_hours' => rand(1, 20),
                    ]);
                }
            }
        }
        // PricedItem::factory()
        //     ->count(500)
        //     ->create();
    }
}
