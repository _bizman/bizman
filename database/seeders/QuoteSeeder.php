<?php

namespace Database\Seeders;

use App\Models\Client;
use App\Models\Quote;
use App\Models\QuoteState;
use App\Models\User;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class QuoteSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $users = User::all();

        foreach ($users as $user) {
            $client = Client::all()->where('user_id', $user->id)->random();
            $state = QuoteState::all()->where('user_id', $user->id)->random();

            DB::table('quotes')->insert([
                'client_id' => $client->id,
                'user_id' => $user->id,
                'state_id' => $state->id,
                'name' => 'Wycena strony internetowej - ' . $client->company,
                'notes' => 'Klient z Poznania'
            ]);
        }
        // Quote::factory()
        //     ->count(150)
        //     ->create();
    }
}
