<?php

namespace Database\Seeders;

use App\Models\QuoteState;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Foundation\Auth\User;
use Illuminate\Support\Facades\DB;

class QuoteStateSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $users = User::all();

        $states = ['Oczekujące', 'W trakcie', 'Akceptacja', 'Wstrzymane', 'Zakończone',  'Anulowane'];

        $colors = ['#F0BE0A',     '#FC6035',     '#00D42E',   '#C226ED',    '#757575', '#AD0505'];
        foreach ($users as $user) {
            foreach ($states as $key => $name) {
                DB::table('quote_states')->insert([
                    'user_id' => $user->id,
                    'state' => $name,
                    'color' => $colors[$key]
                ]);
            }
        }

        // QuoteState::factory()
        //     ->count(20)
        //     ->create();
    }
}
