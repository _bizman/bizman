<?php

namespace Database\Seeders;

use App\Models\PricedItem;
use App\Models\Quote;
use App\Models\QuoteElement;
use App\Models\User;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class QuoteElementSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $users = User::all();

        foreach ($users as $user) {
            $quote = Quote::all()->where('user_id', '=', $user->id)->random();

            $priced_items = PricedItem::all()->where('user_id', '=', $user->id);

            foreach ($priced_items as $key => $element) {
                DB::table('quote_elements')->insert([
                    'user_id' => $user->id,
                    'quote_id' => $quote->id,
                    'job_type_id' => $element->job_type_id,
                    'title' => $element->title,
                    'content' => $element->content,
                    'work_hours' => $element->work_hours,
                ]);
            }
        }

        // QuoteElement::factory()
        //     ->count(500)
        //     ->create();
    }
}
