<x-app-layout>
  @include('partials.toast')

  <form id="client_data" method="POST" action="/clients/{{ $client->id }}">
    @csrf
    @method('PATCH')
    <x-containers.outer title="Edycja: {{ $client->first_name }} {{ $client->last_name }}" buttonStyle="primary" buttonType="submit" buttonText="Zapisz">

      <x-containers.inner title="Dane kontaktowe">
        <div class="grid grid-cols-2 gap-x-4 gap-y-2">
          <x-inputs.text name="first_name" :value="old('first_name', $client->first_name)" label="Imię" errorKey="first_name" />
          <x-inputs.text name="last_name" :value="old('last_name', $client->last_name)" label="Nazwisko" errorKey="last_name" />
          <x-inputs.tel name="phone_number" :value="old('phone_number', $client->phone_number)" label="Numer telefonu" errorKey="phone_number" />
          <x-inputs.email name="email" :value="old('email', $client->email)" label="Adres email" errorKey="email" />
          <x-inputs.textarea name="notes" :value="old('notes', $client->notes)" class="col-span-2" label="Notatki" errorKey="notes" />
        </div>
      </x-containers.inner>

      <x-containers.inner title="Dane firmy">
        <div class="grid grid-cols-2 gap-x-4 gap-y-2">
          <x-inputs.text name="company" :value="old('company', $client->company)" label="Firma" errorKey="company" />
          <x-inputs.text name="tax_id" :value="old('tax_id', $client->tax_id)" label="Regon" errorKey="tax_id" />
          <x-inputs.text name="company_id" :value="old('company_id', $client->company_id)" label="NIP" errorKey="company_id" />
          <x-inputs.text name="website" :value="old('website', $client->website)" label="Strona" errorKey="website" icon="internet" />
        </div>
      </x-containers.inner>

      <x-containers.inner title="Dane adresowe">
        <div class="grid grid-cols-3 gap-x-4 gap-y-2">
          <x-inputs.text name="street" :value="old('street', $client->street)" label="Ulica" errorKey="street" />
          <x-inputs.text name="house" :value="old('house', $client->house)" label="Numer domu" errorKey="house" />
          <x-inputs.text name="house_number" :value="old('house_number', $client->house_number)" label="Numer lokalu" errorKey="house_number" />
          <x-inputs.text name="city" :value="old('city', $client->city)" label="Miasto" errorKey="city" />
          <x-inputs.text name="postal" :value="old('postal', $client->postal)" label="Kod pocztowy" errorKey="postal" />
          <x-inputs.text name="country" :value="old('country', $client->country)" label="Kraj" errorKey="country" />
          <x-inputs.text name="voivodeship" :value="old('voivodeship', $client->voivodeship)" label="Województwo" errorKey="voivodeship" />
          <x-inputs.text name="county" :value="old('county', $client->county)" label="Powiat" errorKey="county" />
          <x-inputs.text name="borough" :value="old('borough', $client->borough)" label="Gmina" errorKey="borough" />
        </div>
      </x-containers.inner>

    </x-containers.outer>
  </form>
</x-app-layout>
