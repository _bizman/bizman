<x-app-layout>

  @include('partials.toast')

  <form id="client_data" method="POST" action="/clients">
    @csrf
    <x-containers.outer title="Nowy klient" buttonStyle="primary" buttonType="submit" buttonText="Dodaj">

      <x-containers.inner title="Dane kontaktowe">
        <div class="grid grid-cols-2 gap-x-4 gap-y-2">
          <x-inputs.text name="first_name" :value="old('first_name')" label="Imię" errorKey="first_name" />
          <x-inputs.text name="last_name" :value="old('last_name')" label="Nazwisko" errorKey="last_name" />
          <x-inputs.tel name="phone_number" :value="old('phone_number')" label="Numer telefonu" errorKey="phone_number" />
          <x-inputs.email name="email" :value="old('email')" label="Adres email" errorKey="email" />
          <x-inputs.textarea name="notes" :value="old('notes')" class="col-span-2" label="Notatki" errorKey="notes" />
        </div>
      </x-containers.inner>

      <x-containers.inner title="Dane firmy">
        <div class="grid grid-cols-2 gap-x-4 gap-y-2">
          <x-inputs.text name="company" :value="old('company')" label="Firma" errorKey="company" />
          <x-inputs.text name="tax_id" :value="old('tax_id')" label="Regon" errorKey="tax_id" />
          <x-inputs.text name="company_id" :value="old('company_id')" label="NIP" errorKey="company_id" />
          <x-inputs.text name="website" :value="old('website')" label="Strona" errorKey="website" icon="internet" />
        </div>
      </x-containers.inner>

      <x-containers.inner title="Dane adresowe">
        <div class="grid grid-cols-3 gap-x-4 gap-y-2">
          <x-inputs.text name="street" :value="old('street')" label="Ulica" errorKey="street" />
          <x-inputs.text name="house" :value="old('house')" label="Numer domu" errorKey="house" />
          <x-inputs.text name="house_number" :value="old('house_number')" label="Numer lokalu" errorKey="house_number" />
          <x-inputs.text name="city" :value="old('city')" label="Miasto" errorKey="city" />
          <x-inputs.text name="postal" :value="old('postal')" label="Kod pocztowy" errorKey="postal" />
          <x-inputs.text name="country" :value="old('country')" label="Kraj" errorKey="country" />
          <x-inputs.text name="voivodeship" :value="old('voivodeship')" label="Województwo" errorKey="voivodeship" />
          <x-inputs.text name="county" :value="old('county')" label="Powiat" errorKey="county" />
          <x-inputs.text name="borough" :value="old('borough')" label="Gmina" errorKey="borough" />
        </div>
      </x-containers.inner>

    </x-containers.outer>
  </form>
</x-app-layout>
