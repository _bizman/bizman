<x-app-layout>
  @include('partials.toast')
  <x-containers.outer title="{{ $client->first_name }} {{ $client->last_name }}" buttonStyle="secondary" :buttonLink="url('/clients/' . $client->id . '/edit')" buttonText="Edycja">
    <div class="grid grid-cols-2 gap-16">

      <div class="flex flex-col">
        <x-containers.inner title="Dane kontaktowe">
          <div class="grid grid-cols-2 gap-x-4 gap-y-2">
            <x-inputs.text name="first_name" :value="$client->first_name" label="Imię" :disabled="true" errorKey="first_name" />
            <x-inputs.text name="last_name" :value="$client->last_name" label="Nazwisko" :disabled="true" errorKey="last_name" />
            <x-inputs.tel name="phone_number" :value="$client->phone_number" label="Numer telefonu" :disabled="true" errorKey="phone_number" />
            <x-inputs.email name="email" :value="$client->email" label="Adres email" errorKey="email" :disabled="true" />
            <x-inputs.textarea name="notes" :value="$client->notes" class="col-span-2" label="Notatki" errorKey="notes" :disabled="true" />
            <x-inputs.text name="created_at" :value="\Carbon\Carbon::parse($client->created_at)->format('d.m.Y - H:m:s')" :disabled="true" label="Data dodania" />
            <x-inputs.text name="updated_at" :value="\Carbon\Carbon::parse($client->updated_at)->format('d.m.Y - H:m:s')" :disabled="true" label="Ostatnia aktualizacja" />
          </div>
        </x-containers.inner>

        <x-containers.inner title="Dane firmy">
          <div class="grid grid-cols-2 gap-x-4 gap-y-2">
            <x-inputs.text name="company" :value="$client->company" label="Firma" :disabled="true" errorKey="company" />
            <x-inputs.text name="tax_id" :value="$client->tax_id" label="Regon" :disabled="true" errorKey="tax_id" />
            <x-inputs.text name="company_id" :value="$client->company_id" label="NIP" :disabled="true" errorKey="company_id" />
            <span class="flex flex-col w-full ">
              <span class="mb-1 text-sm font-semibold text-gray-500">Strona</span>
              <a title="Przejdź do strony" target="_blank" class="block px-4 py-2 text-black bg-white rounded-lg" href="https://{{ $client->website }}">{{ $client->website }}</a>
            </span>
          </div>
        </x-containers.inner>

        <x-containers.inner title="Dane adresowe">
          <div class="grid grid-cols-2 gap-x-4 gap-y-2">
            <x-inputs.text name="street" :value="$client->street" label="Ulica" :disabled="true" errorKey="street" />
            <x-inputs.text name="house" :value="$client->house" label="Numer domu" :disabled="true" errorKey="house" />
            <x-inputs.text name="house_number" :value="$client->house_number" label="Numer lokalu" :disabled="true" errorKey="house_number" />
            <x-inputs.text name="city" :value="$client->city" label="Miasto" :disabled="true" errorKey="city" />
            <x-inputs.text name="postal" :value="$client->postal" label="Kod pocztowy" :disabled="true" errorKey="postal" />
            <x-inputs.text name="country" :value="$client->country" label="Kraj" :disabled="true" :disabled="true" errorKey="country" />
            <x-inputs.text name="voivodeship" :value="$client->voivodeship" label="Województwo" :disabled="true" errorKey="voivodeship" />
            <x-inputs.text name="county" :value="$client->county" label="Powiat" :disabled="true" errorKey="county" />
            <x-inputs.text name="borough" :value="$client->borough" label="Gmina" :disabled="true" errorKey="borough" />
          </div>
        </x-containers.inner>
      </div>

      <x-containers.inner title="Lista wycen">
        @if (count($quotes) > 0)
          <ul class="flex flex-col gap-4 max-h-[600px] overflow-auto">
            @foreach ($quotes as $quote)
              <li class="">
                <a href="{{ url('/quotes/' . $quote->id) }}" class="block px-8 py-4 transition-all bg-white rounded-md hover:bg-blue hover:text-white">
                  <div class="flex gap-4 mb-4">
                    <div class="text-xs created-at">
                      <span>Utworzono:</span> <span class="font-semibold">{{ \Carbon\Carbon::parse($quote->created_at)->format('d.m.Y') }}</span>
                    </div>
                    <div class="text-xs updated-at">
                      <span>Ostatnia aktualizacja:</span> <span class="font-semibold">{{ \Carbon\Carbon::parse($quote->updated_at)->format('d.m.Y') }}</span>
                    </div>
                  </div>
                  <div class="text-base">
                    {{ $quote->name }}
                  </div>
                </a>
              </li>
            @endforeach
          </ul>
        @else
          <div class="empty">
            {{ __('Brak wycen') }}
          </div>
        @endif ($quotes)
      </x-containers.inner>
    </div>

  </x-containers.outer>
</x-app-layout>
