<x-app-layout>
  @include('partials.toast')
  <form id="priceditems_data" method="POST" action="/priceditems">
    @csrf
    <x-containers.outer title="Tworzenie nowego elementu" buttonStyle="primary" buttonType="submit" buttonText="Dodaj">
      <x-containers.inner title="">
        <div class="grid grid-cols-2 gap-4">
          <x-inputs.text name="title" :value="old('title')" labelClass="col-span-2" label="Tytuł" errorKey="title" required="true" />
          <x-inputs.number name="work_hours" :value="old('work_hours')" label="Godziny" errorKey="work_hours" required="true" />
          <x-inputs.select name="job_type_id" label="Rodzaj prac" errorKey="job_type_id" required="true">
            @foreach ($types as $type)
              <option value="{{ $type->id }}">
                {{ $type->type }}
              </option>
            @endforeach
          </x-inputs.select>
          <x-inputs.textarea name="content" :value="old('content')" class="col-span-2" label="Treść" errorKey="content" />
        </div>
      </x-containers.inner>
    </x-containers.outer>
  </form>
</x-app-layout>
