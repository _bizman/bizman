<x-app-layout>
  @include('partials.toast')
  <form id="priceditems_data" method="POST" action="/priceditems/{{ $pricedItem->id }}">
    @csrf
    @method('PATCH')
    <x-containers.outer title="Tworzenie nowego elementu" buttonStyle="primary" buttonType="submit" buttonText="Zapisz">
      <x-containers.inner title="">
        <div class="grid grid-cols-2 gap-4">
          <x-inputs.text name="title" :value="$pricedItem->title" labelClass="col-span-2" label="Tytuł" errorKey="title" required="true" />
          <x-inputs.number name="work_hours" :value="$pricedItem->work_hours" label="Godziny" errorKey="work_hours" required="true" />
          <x-inputs.select name="job_type_id" label="Rodzaj prac" errorKey="job_type_id" required="true">
            @foreach ($types as $type)
              <option value="{{ $type->id }}" @if ($pricedItem->jobType->id == $type->id) {{ 'selected' }} @endif>
                {{ $type->type }}
              </option>
            @endforeach
          </x-inputs.select>
          <div class="col-span-2">
            <x-inputs.textarea name="content" :value="$pricedItem->content" inputClass="tinymce invisible" label="Treść" errorKey="content" />
          </div>
        </div>
      </x-containers.inner>
    </x-containers.outer>
  </form>
</x-app-layout>
