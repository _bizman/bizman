<x-app-layout>
  @include('partials.toast')

  <x-containers.outer :title="$quote->name" buttonStyle="secondary" :buttonLink="url('/quotes/' . $quote->id . '/edit')" buttonText="Edycja">
    <x-containers.inner title="Szczegóły">
      <x-inputs.text label="Status" value="{{ $quote->state->state }}" disabled="true" />
      <x-inputs.text label="Klient" value="{{ $quote->client->first_name . ' ' . $quote->client->last_name }}" disabled="true" />
      <p class="flex flex-col gap-1 text-sm font-semibold text-gray-500">Notatka</p>
      <div class="max-w-full p-4 prose bg-white rounded-lg notes max-h-[230px] overflow-auto h-full">
        {!! $quote->notes !!}
      </div>
    </x-containers.inner>

    <div class="grid 3xl:grid-cols-[720px_auto] grid-cols-1 gap-16">

      <x-containers.inner title="Estymacja czasowa">

        <form action="/quotes/{{ $quote->id }}/calculate" method="POST">

          @csrf
          @method('PATCH')

          <div class="flex flex-col gap-4 calculator">
            @if ($jobs)
              @foreach ($jobs as $job)
                <div class="p-4 pb-4 bg-gray-200 rounded-lg job-type-wrapper">
                  <x-titles.text>{{ $job['type'] }}</x-titles.text>
                  <div class="flex gap-4 mt-4">
                    <x-inputs.number inputClass="hours" :value="$job['time']" label="Godziny" disabled="true" labelClass="max-w-[80px]" />
                    <x-inputs.number inputClass="per-hour" name="calculator[{{ $loop->index }}][perhour]" value="{{ $calculate != null ? $calculate[$loop->index]['perhour'] : 200 }}" label="Stawka" suffix="zł" labelClass="max-w-[130px]" />
                    <x-inputs.number min="0" max="-100" name="calculator[{{ $loop->index }}][negative]" inputClass="negative" value="{{ $calculate != null ? $calculate[$loop->index]['negative'] : -10 }}" label="Wariant -" suffix="%"
                      labelClass="max-w-[120px]" />
                    <x-inputs.number min="0" max="100" name="calculator[{{ $loop->index }}][positive]" inputClass="positive" value="{{ $calculate != null ? $calculate[$loop->index]['positive'] : 10 }}" label="Wariant +" suffix="%"
                      labelClass="max-w-[120px]" />
                    <x-inputs.number inputClass="row-total" label="Suma" disalbed="true" suffix="zł" labelClass="max-w-[200px]" disabled="true" />
                    <x-inputs.hidden value="{{ $job['id'] }}" name="calculator[{{ $loop->index }}][job_id]" />
                  </div>
                </div>
              @endforeach
            @endif
          </div>
          <x-button class="hidden mt-4 ml-auto save-button" style="btn-secondary">Zapisz</x-button>
        </form>

        <div class="flex gap-8 pt-4 calculator-summary">
          <x-link target="_blank" href="{{ url('/quotes/' . $quote->id . '/pdf') }}" class="generate-pdf w-fit h-fit">PDF</x-link>
          <div class="flex gap-4 p-4 rounded-lg bg-yellow/20 grow">
            <x-inputs.number inputClass="total-negative" label="Wariant -" disabled="true" suffix="zł" />
            <x-inputs.number inputClass="total" label="Suma" disabled="true" suffix="zł" />
            <x-inputs.number inputClass="total-positive" label="Wariant +" disabled="true" suffix="zł" />
          </div>
        </div>

      </x-containers.inner>
      <x-containers.inner title="Elementy wyceny {{ '(' . $quote->pricedItems->count() . ')' }}">

        <div class="px-6 py-6 bg-white max-h-[850px] mb-4 overflow-auto rounded-lg">
          @foreach ($quote->pricedItems as $item)
            <div class="priced-item bg-[#F2F2F2] px-6 py-6 rounded-lg mb-4 ">
              <div class="grid grid-cols-[auto_50px]">
                <div class="flex flex-col">
                  <div class="mb-2 font-semibold title"> {{ $item->title }}</div>
                  <div class="prose content">
                    {!! $item->content !!}
                  </div>
                </div>
                <div class="flex flex-col items-center gap-2 border-l border-gray-300 job">
                  <div class="font-bold text-center uppercase abb" title="{{ $item->jobType->type }}">
                    {{ $item->jobType->abbreviation }}
                  </div>
                  <div class="text-center hours">
                    {{ $item->work_hours }}
                  </div>
                </div>
              </div>
            </div>
          @endforeach
        </div>
      </x-containers.inner>
    </div>
  </x-containers.outer>
</x-app-layout>
