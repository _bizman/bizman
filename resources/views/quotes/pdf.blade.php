<!DOCTYPE html>
<html>

<head>
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
  <link href="https://fonts.googleapis.com/css2?family=Montserrat:wght@300;400;700" rel="stylesheet" />
  <title>Bizman - wycena</title>
  <style>
    body {
      font-family: 'Montserrat';
    }

    @page {
      margin-top: 120px;
      margin-bottom: 80px;
    }

    header,
    footer {
      position: fixed;
      left: 0px;
      right: 0px;
    }

    header {
      height: 60px;
      margin-top: -90px;
      border-bottom: 1px rgb(209 213 219) solid;
    }

    footer {
      bottom: 0;
      height: 40px;
      margin-bottom: -60px;
      border-top: 1px rgb(209 213 219) solid;
    }

    .front-page {
      position: absolute;
      top: 50%;
      left: 50%;
      transform: translate(-50%, -50%);
    }

    .page-break {
      page-break-after: always;
    }

    .generated {
      position: absolute;
      top: 0;
      right: 0;
    }

    .expiry {
      position: absolute;
      left: 0;
      bottom: 50px;
    }

    .single-item {
      page-break-inside: avoid;
      margin-bottom: 60px;
    }

    span {
      font-size: 14px;
      color: rgb(156 163 175);
    }

    .page-number:after {
      content: "Strona: "counter(page);
      position: absolute;
      top: 0;
      right: 0;
    }

    .title {
      font-family: 'Montserrat';
      font-size: 22px;
      font-weight: 700;
    }
  </style>
</head>

<body>
  <div class="front-page page-break">
    <?php
    $path = 'images/pdf-logo.jpg';
    $type = pathinfo($path, PATHINFO_EXTENSION);
    $data = file_get_contents($path);
    $base64 = 'data:image/' . $type . ';base64,' . base64_encode($data);
    ?>
    <img src="<?php echo $base64; ?>" width="578" height="165" />
    <p style="font-size:40px;">{{ $quote->name }}</p>
    <p style="font-size:20px;">Klient: {{ $client->first_name }} {{ $client->last_name }}</p>
  </div>

  <header>
    <?php
    $path = 'images/pdf-header.jpg';
    $type = pathinfo($path, PATHINFO_EXTENSION);
    $data = file_get_contents($path);
    $base64 = 'data:image/' . $type . ';base64,' . base64_encode($data);
    ?>
    <img src="<?php echo $base64; ?>" width="193" height="55" />
    <span class="generated">Wygenerowano: <?php echo date('Y-m-d'); ?></span>
  </header>

  <footer>
    <span>&copy; <?php echo date('Y'); ?> Bizman</span>
    <span class="page-number"></span>
    <span class="expiry">*Wycena ważna rok od daty wygenerowania.</span>
  </footer>

  <main>
    <ol>
      @foreach ($quote->pricedItems as $key => $item)
        <li>
          {!! $item->content !!}
        </li>
      @endforeach
    </ol>
    <div class="single-item">
      <h2 class="title">Podsumowanie</h2>
      <h4 class="small-title">Podział na rodzaj wykonywanych prac (godziny):</h4>
      @foreach ($quote->hours_summary as $slug => $item)
        @if ($item['hours'] != 0)
          <p>
            {{ trim($item['job']) }} - {{ $item['hours'] }}
          </p>
        @endif
      @endforeach
      <h4 class="small-title">Stawki:</h4>
      @foreach ($quote->hours_summary as $slug => $item)
        @if ($item['hours'] != 0)
          <p>
            {{ trim($item['job']) }} - {{ $item['per_hour'] }}&nbsp;zł
          </p>
        @endif
      @endforeach
      <h4 class="small-title">Koszt:</h4>

      @php
        $totalNegative = 0;
        $totalPositive = 0;

        foreach ($quote->hours_summary as $slug => $item) {
            $total = $item['hours'] * $item['per_hour'];
            $totalNegative += $total + $total * ($item['negative'] / 100);
            $totalPositive += $total + $total * ($item['positive'] / 100);
        }
        $fmt = numfmt_create('pl_PL', NumberFormatter::CURRENCY);

      @endphp
      @if ($totalNegative != $totalPositive)
        <p class="title">
          <small style="font-weight:400">Od</small> {{ numfmt_format_currency($fmt, $totalNegative, 'PLN') }} <small style="font-weight:400">do</small> {{ numfmt_format_currency($fmt, $totalPositive, 'PLN') }}
        </p>
      @else
        <p class="title">
          {{ numfmt_format_currency($fmt, $totalNegative, 'PLN') }}&nbsp;zł
        </p>
      @endif
    </div>
  </main>
</body>

</html>
