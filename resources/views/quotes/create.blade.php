<x-app-layout>

  @include('partials.toast')

  <form method="POST" action="/quotes">
    @csrf
    <x-containers.outer title="Nowa wycena" buttonStyle="primary" buttonType="submit" buttonText="Dodaj wycenę">

      <x-containers.inner title="Dane wyceny">
        <x-inputs.text name="name" :value="old('name')" label="Tytuł" errorKey="name" required="true" />
        <x-inputs.select name="state_id" id="state_id" label="Status" errorKey="state_id" required="true">
          <option value="" {{ !old('state_id') ? 'selected' : '' }}>{{ __('Wybierz status') }}</option>
          @foreach ($states as $state)
            <option value="{{ $state->id }}" {{ old('state_id') == $state->id ? 'selected' : '' }}>
              {{ $state->state }}</option>
          @endforeach
        </x-inputs.select>
        <x-inputs.select name="client_id" id="client_id" label="Klient" errorKey="client_id" required="true">
          <option value="" {{ !old('client_id') ? 'selected' : '' }}>{{ __('Wybierz klienta') }}</option>
          @foreach ($clients as $client)
            <option value="{{ $client->id }}" {{ old('client_id') == $client->id ? 'selected' : '' }}>{{ $client->first_name }} {{ $client->last_name }}</option>
          @endforeach
        </x-inputs.select>
        <x-inputs.textarea name="notes" :value="old('notes')" class="col-span-2" label="Notatki" errorKey="notes" />
      </x-containers.inner>


      <x-containers.inner title="Elementy wyceny">
        <div class="mb-4 ml-auto -mt-8 cursor-pointer btn-primary add-new-item">Dodaj nowy
          <x-icons.add />
        </div>
        <ul class="max-h-[600px] min-h-[200px] h-full overflow-auto bg-white rounded-lg px-6 py-6  gap-4 flex flex-col quote-list mb-8">
        </ul>
      </x-containers.inner>
    </x-containers.outer>
  </form>
  <section class="container mx-auto sm:my-12 mb-[120px] mt-[32px]  border border-[#D5DAE1] rounded-xl px-12 py-8 relative">
    <h1 class="text-2xl font-semibold leading-tight text-[#6A7C96] mb-16">Wycenione elementy</h1>
    <x-containers.inner>
      <x-inputs.text inputClass="quote-search" labelClass="max-w-[450px] w-full ml-auto -mt-4 mb-4" placeholder="Szukaj..." />
      <ul class="max-h-[800px] overflow-auto bg-white rounded-lg px-6 py-6  gap-4 flex flex-col priced-list">
        @if ($priced_items->isNotEmpty())
          @foreach ($priced_items->reverse() as $item)
            @php
              $search_data = [
                  'title' => mb_strtolower(strip_tags($item->title)),
                  'content' => mb_strtolower(strip_tags($item->content)),
              ];
              $copy_data = [
                  'title' => $item->title,
                  'content' => $item->content,
                  'job_type_id' => $item->jobType->id,
                  'work_hours' => $item->work_hours,
              ];
            @endphp
            <li id="item_{{ $loop->index }}" class="priced-item-wrapper grid grid-cols-[auto_250px_75px] bg-gray-100 hover:border-blue border border-gray-100  rounded-lg gap-6 py-2 px-2 item transition-all" data-copy="{{ json_encode($copy_data) }}"
              data-search="{{ json_encode($search_data) }}">
              <div class="pl-4 item">
                <x-titles.text class="mt-4 mb-3">
                  {{ $item->title }}
                </x-titles.text>
                <div class="max-w-full p-4 mb-4 prose bg-white rounded-lg">
                  {!! $item->content !!}
                </div>
              </div>
              <div class="flex flex-col gap-4 my-5">
                <x-inputs.text label="Kategoria" :value="$item->jobType->type" disabled="true" />
                <x-inputs.text label="Godziny" :value="$item->work_hours" disabled="true" />

              </div>
              <div class="flex items-center justify-center w-full h-full transition-all bg-white rounded-lg shadow cursor-pointer add-to-quote hover:bg-blue hover:text-white" title="Dodaj do wyceny">
                <x-icons.add />
              </div>
            </li>
          @endforeach
        @endif
      </ul>
    </x-containers.inner>
  </section>

  <template id="item_template">
    <li class="grid grid-cols-[auto_250px_75px] bg-gray-100 hover:border-blue border border-gray-100  rounded-lg gap-6 py-2 px-2 item transition-all">
      <div class="flex flex-col gap-4 py-5 pl-4">
        <x-inputs.text inputClass="title" label="Tytuł" required="true" />
        <x-inputs.textarea inputClass="content" label="Opis" />
      </div>
      <div class="flex flex-col gap-4 my-5">
        <x-inputs.number inputClass="work-hours" label="Godziny" required="true" />
        <x-inputs.select inputClass="job-type-id" label="Kategoria" required="true">
          @if ($job_types)
            @foreach ($job_types as $jobtype)
              <option value="{{ $jobtype->id }}">{{ $jobtype->type }}</option>
            @endforeach
          @endif
        </x-inputs.select>
        <x-inputs.checkbox inputClass="add-to-priced" label="Dodaj do wycenionych" />
      </div>
      <div class="flex items-center justify-center w-full h-full transition-all bg-white rounded-lg shadow cursor-pointer remove-from-quote hover:bg-yellow hover:text-white" title="Usuń z wyceny">
        <x-icons.remove />
      </div>
    </li>
  </template>

</x-app-layout>
