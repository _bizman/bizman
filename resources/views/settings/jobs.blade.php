<x-app-layout>
  @include('partials.toast')
  @include('settings.partials.navigation')

  <form method="POST" action="/user/settings/categories">
    @csrf
    @method('PATCH')
    <x-containers.outer title="Lista kategorii" buttonStyle="primary" buttonType="submit" buttonText="Zapisz zmiany">
      <x-containers.inner>


        @if ($job_types->isNotEmpty())
          <div class="grid grid-cols-[auto_180px_50px] gap-4 mb-2 ">
            <x-titles.text>Nazwa</x-titles.text>
            <x-titles.text>Skrót</x-titles.text>
            <x-titles.text>Usuń</x-titles.text>

          </div>
          <ul id="job_type_list">
            @foreach ($job_types as $job)
              <li class="grid grid-cols-[auto_180px_50px] gap-4 mb-2 ">
                <x-inputs.text labelClass="job-type" placeholder="Nazwa" name="jobtypes[{{ $loop->index }}][type]" :value="$job->type" :errorKey="'jobtypes.' . $loop->index . '.type'" :required="true" />
                <x-inputs.text labelClass="job-abbreviation" placeholder="Nazwa" name="jobtypes[{{ $loop->index }}][abbreviation]" :value="$job->abbreviation" :errorKey="'jobtypes.' . $loop->index . '.abbreviation'" :required="true" />
                <x-inputs.hidden class="job-id" name="jobtypes[{{ $loop->index }}][id]" :value="$job->id" />
                <x-inputs.checkbox labelClass="job-delete cursor-pointer pt-3" name="jobtypes[{{ $loop->index }}][delete]" class="cursor-pointer" />
              </li>
            @endforeach
          </ul>
        @endif

        <x-buttons.primary class="mx-auto" id="new_job_type">
          Dodaj nowy
          <x-icons.add />
        </x-buttons.primary>


      </x-containers.inner>
    </x-containers.outer>
  </form>

  <template id="job_type_input_template">
    <li class="grid grid-cols-[auto_180px_50px] gap-4 mb-2 new-job">
      <x-inputs.text labelClass="job-type" placeholder="Nazwa" :required="true" />
      <x-inputs.text labelClass="job-abbreviation" placeholder="Nazwa" :required="true" />
      <div class="flex items-center justify-center cursor-pointer remove">
        <div class="flex items-center justify-center w-4 h-4 font-medium text-white bg-red-500 pointer-events-none">-</div>
      </div>
    </li>
  </template>

  <template id="delete_info">
    <div class="delete-info">
      <div class="p-4 mt-6 bg-white border rounded-lg border-yellow">
        <h2 class="block mb-2">Uwaga</h2>
        <div class="mb-2 text-sm text-gray-700">
          <p class="mb-2">Usuwasz kategorię. Wybierz do jakiej kategorii przypisać wycenione elementy.</p>
        </div>
      </div>
      <x-inputs.select labelClass="mt-4" name="default_job_type" label="Domyślna kategoria" :required="true">
        @if ($job_types->isNotEmpty())
          @foreach ($job_types as $job)
            <option value="{{ $job->id }}">{{ $job->type }}</option>
          @endforeach
        @endif
      </x-inputs.select>
    </div>
  </template>

</x-app-layout>
