<section class="container mx-auto mt-12">
  <div class="flex gap-2 navigation">
    @foreach ($tabs as $routeName => $label)
      @if ($routeName == $activeTab)
        <span class="{{ $activeClasses }}">{{ $label }}</span>
      @else
        <a href="{{ route($routeName) }}" class="{{ $tabClasses }}">{{ $label }}</a>
      @endif
    @endforeach
  </div>
</section>
