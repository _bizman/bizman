<x-app-layout>
  @include('partials.toast')
  @include('settings.partials.navigation')

  <form method="POST" action="/user/settings/user">
    @csrf
    @method('PATCH')
    <x-containers.outer title="Twoje dane" buttonStyle="primary" buttonType="submit" buttonText="Zapisz zmiany">
      <x-containers.inner>
        <div class="flex flex-col gap-4">
          <x-inputs.text class="" label="Imię i nazwisko" name="user_name" value="{{ Auth::user()->name }}" errorKey="user_name" autocomplete="off" />
          <x-inputs.email class="" label="Twój adres email" name="user_email" value="{{ Auth::user()->email }}" errorKey="user_email" autocomplete="off" />
        </div>
      </x-containers.inner>

    </x-containers.outer>
  </form>


</x-app-layout>
