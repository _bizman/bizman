<x-app-layout>
  @include('partials.toast')
  @include('settings.partials.navigation')

  <form method="POST" action="/user/settings/password">
    @csrf
    @method('PATCH')
    <x-containers.outer title="Zmiana hasła" buttonStyle="primary" buttonType="submit" buttonText="Zapisz zmiany">
      <x-containers.inner>
        <div class="flex flex-col gap-2">
          <x-inputs.password class="" name="current_password" value="" label="Obecne hasło" errorKey="current_password" autocomplete="current-password" />

          <x-inputs.password class="" name="user_password" value="" label="Nowe hasło" errorKey="user_password" autocomplete="new-password" />
          <x-inputs.password class="" name="user_password_confirmation" value="" label="Powtórz nowe hasło" errorKey="user_password_confirmation" autocomplete="new-password" />
          <div class="p-4 mt-6 bg-white border rounded-lg border-yellow">
            <h2 class="block mb-2">Uwagi</h2>
            <div class="mb-2 text-sm text-gray-700">
              <p class="mb-2">Hasło musi mieć conajmniej:</p>
              <ul class="pl-6 list-disc">
                <li>minimalną długość 8 znaków</li>
                <li>wielkie i małe litery</li>
                <li>conajmniej jeden znak specjalny</li>
                <li>conajmniej jedną liczbę</li>
              </ul>
            </div>
          </div>
        </div>
      </x-containers.inner>
    </x-containers.outer>
  </form>

</x-app-layout>
