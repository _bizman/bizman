<x-app-layout>
  @include('partials.toast')
  @include('settings.partials.navigation')


  <form method="POST" action="/user/settings/statuses">
    @csrf
    @method('PATCH')
    <x-containers.outer title="Ustawienia" buttonStyle="primary" buttonType="submit" buttonText="Zapisz zmiany">
      <x-containers.inner>

        <span id="quote_states_wrapper">
          <div class="mb-2  gap-4 grid grid-cols-[auto_50px_50px]">
            <x-titles.text>Nazwa</x-titles.text>
            <x-titles.text>Kolor</x-titles.text>
            <x-titles.text>Usuń</x-titles.text>
          </div>
          @if ($quote_statuses->isNotEmpty())
            <ul id="quote_statuses_list">
              @foreach ($quote_statuses as $status)
                <li class="setting_option">
                  <div class="mb-2 state-wrapper gap-4 grid grid-cols-[auto_50px_50px]">
                    <x-inputs.text labelClass="status-state" placeholder="Nazwa statusu" name="statuses[{{ $loop->index }}][state]" :value="$status->state" :errorKey="'statuses.' . $loop->index . '.name'" :required="true" />
                    <x-inputs.color labelClass="status-color rounded-lg overflow-hidden cursor-pointer" placeholder="Nazwa statusu" name="statuses[{{ $loop->index }}][color]" :value="$status->color" :errorKey="'statuses.' . $loop->index . '.color'" />
                    <x-inputs.hidden class="status-id" name="statuses[{{ $loop->index }}][id]" :value="$status->id" />
                    <x-inputs.checkbox labelClass="status-delete cursor-pointer pt-3" name="statuses[{{ $loop->index }}][delete]" class="cursor-pointer" />
                  </div>
                </li>
              @endforeach
            </ul>
          @endif
        </span>

        <x-buttons.primary class="mx-auto" id="new_status">
          Dodaj nowy
          <x-icons.add />
        </x-buttons.primary>
      </x-containers.inner>

    </x-containers.outer>
  </form>

  <template id="job_status_input_template">
    <li class="grid grid-cols-[auto_50px_50px] gap-4 mb-2 new-status">
      <x-inputs.text labelClass="status-state" placeholder="Nazwa statusu" :required="true" />
      <x-inputs.color labelClass="status-color rounded-lg overflow-hidden" value="#000000" />
      <div class="flex items-center justify-center cursor-pointer remove">
        <div class="flex items-center justify-center w-4 h-4 font-medium text-white bg-red-500 pointer-events-none">-</div>
      </div>
    </li>
  </template>

  <template id="delete_info">
    <div class="delete-info">
      <div class="p-4 mt-6 bg-white border rounded-lg border-yellow">
        <h2 class="block mb-2">Uwaga</h2>
        <div class="mb-2 text-sm text-gray-700">
          <p class="mb-2">Usuwasz status. Wybierz do jakiego statusu przypisać wyceny.</p>
        </div>
      </div>
      <x-inputs.select labelClass="mt-4" name="default_status" label="Domyślna kategoria" :required="true">
        @if ($quote_statuses->isNotEmpty())
          @foreach ($quote_statuses as $status)
            <option value="{{ $status->id }}">{{ $status->state }}</option>
          @endforeach
        @endif
      </x-inputs.select>
    </div>
  </template>

</x-app-layout>
