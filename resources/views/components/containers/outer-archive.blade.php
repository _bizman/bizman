 @props([
    'title' => '',
    'buttonStyle' => false,
    'buttonText' => false,
    'buttonLink' => '',
    'searchAction' => '',
])
 <div class="container mx-auto sm:my-12 mb-[120px] mt-[32px]  border border-[#D5DAE1] rounded-xl px-12 py-8 relative opacity-0 translate-y-[50px]" data-aos="fade-up" data-aos-duration="150">
   <div class="flex items-center gap-12 mb-16">
     <h1 class="text-2xl font-semibold leading-tight text-[#6A7C96] ">{{ $title }}</h1>
     @if ($buttonStyle == 'secondary')
       <a href="{{ $buttonLink }}">
         <x-buttons.secondary>
           {{ $buttonText }}
         </x-buttons.secondary>
       </a>
     @elseif($buttonStyle == 'primary')
       <a href="{{ $buttonLink }}">
         <x-buttons.primary>
           {{ $buttonText }}
         </x-buttons.primary>
       </a>
     @endif

     <x-inputs.search action="{{ $searchAction }}" placeholder="Szukaj" name="search"></x-inputs.search>
   </div>
   {{ $slot }}
 </div>
