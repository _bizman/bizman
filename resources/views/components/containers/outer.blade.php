 @props([
    'title' => '',
    'buttonStyle' => false,
    'buttonText' => false,
    'buttonLink' => false,
    'buttonType' => 'button',
])
 <div class="container mx-auto sm:my-12 mb-[120px] mt-[32px]  border border-[#D5DAE1] rounded-xl px-12 py-8 relative opacity-0 " data-aos="fade-in" data-aos-duration="150">
   <h1 class="text-2xl font-semibold leading-tight text-[#6A7C96] mb-16">{{ $title }}</h1>
   @if ($buttonStyle == 'secondary')
     @if ($buttonLink)
       <a href="{{ $buttonLink }}" class="absolute top-5 right-7">
         <x-buttons.secondary type="{{ $buttonType }}">
           {{ $buttonText }}
         </x-buttons.secondary>
       </a>
     @else
       <x-buttons.secondary type="{{ $buttonType }}" class="absolute top-5 right-7">
         {{ $buttonText }}
       </x-buttons.secondary>
     @endif
   @elseif($buttonStyle == 'primary')
     @if ($buttonLink)
       <a href="{{ $buttonLink }}" class="absolute top-5 right-7">
         <x-buttons.primary type="{{ $buttonType }}">
           {{ $buttonText }}
         </x-buttons.primary>
       </a>
     @else
       <x-buttons.primary type="{{ $buttonType }}" class="absolute top-5 right-7">
         {{ $buttonText }}
       </x-buttons.primary>
     @endif
   @endif
   {{ $slot }}
 </div>
