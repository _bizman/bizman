@props([
    'id' => false,
    'class' => '',
    'type' => 'button',
])
<button {{ $attributes->merge(['id' => $id, 'type' => $type]) }} {{ $attributes->merge(['class' => 'btn-tertiary ' . $class]) }}>{{ $slot }}</button>