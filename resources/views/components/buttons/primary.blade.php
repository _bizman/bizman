@props([
    'id' => false,
    'class' => '',
    'type' => 'button',
])
<button {{ $attributes->merge(['id' => $id, 'type' => $type, 'class' => 'btn-primary disabled:opacity-50 ' . $class]) }}>{{ $slot }}</button>
