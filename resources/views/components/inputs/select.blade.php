@props([
    'name' => '',
    'disabled' => false,
    'inputClass' => '',
    'labelClass' => '',
    'label' => '',
    'required' => false,
    'errorKey' => '',
])
<label {{ $attributes->merge(['class' => 'text-gray-500 text-sm font-semibold flex flex-col gap-1 ' . $labelClass]) }}>
  @if ($label != '')
    <div>{{ $label }}</div>
  @endif
  <select {{ $attributes->merge(['class' => 'w-full border-none rounded-lg h-10 text-black ' . $inputClass]) }} name="{{ $name }}" {{ $disabled ? 'disabled' : '' }}" {{ $required ? 'required' : '' }}>
    {{ $slot }}
  </select>
  @error($errorKey)
    <span class="text-xs text-red-700">{{ $message }}</span>
  @enderror
</label>
