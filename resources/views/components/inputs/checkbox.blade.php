 @props([
    'name' => '',
    'value' => '',
    'disabled' => false,
    'inputClass' => '',
    'labelClass' => '',
    'label' => '',
    'datasets' => [],
    'errorKey' => '',
])
 <label {{ $attributes->merge(['class' => 'flex justify-center items-center gap-2  h-fit ' . $labelClass]) }}>
   <input type="checkbox" {{ $attributes->merge(['class' => 'disabled:bg-gray-300 disabled:cursor-not-allowed ' . $inputClass]) }} name="{{ $name }}"
     @if (!empty($datasets)) @foreach ($datasets as $dataset){{ ' data-' . $dataset[0] . '=' . $dataset[1] . ' ' }}@endforeach @endif {{ $disabled ? 'disabled' : '' }}>
   @if ($label != '')
     <span>{{ $label }}</span>
   @endif
   {{ $slot }}
   @error($errorKey)
     <span class="text-xs text-red-700">{{ $message }}</span>
   @enderror
 </label>
