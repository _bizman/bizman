 @props([
    'name' => '',
    'placeholder' => '',
    'value' => '',
    'disabled' => false,
    'inputClass' => '',
    'labelClass' => '',
    'label' => '',
    'prefix' => false,
    'suffix' => false,
    'errorKey' => '',
    'required' => '',
])
 <label {{ $attributes->merge(['class' => 'text-gray-500 text-sm font-semibold flex flex-col gap-1 ' . $labelClass]) }}>
   @if ($label != '')
     <div>{{ $label }}</div>
   @endif
   <span class="relative flex overflow-hidden border-gray-200 rounded-lg shadow-xs">
     @if ($prefix)
       <span class="prefix bg-[#D5DAE1]  text-sm text-[#5E5E5E] flex justify-center items-center px-3 font-medium ">{{ $prefix }}</span>
     @endif
     <input type="color" {{ $attributes->merge(['class' => 'w-full min-h-[40px] ' . $inputClass]) }} placeholder="{{ $placeholder }}" name="{{ $name }}" value="{{ $value }}" {{ $disabled ? 'disabled' : '' }} {{ $required ? 'required' : '' }}>
     @if ($suffix)
       <span class="suffix bg-[#D5DAE1] text-sm text-[#5E5E5E] flex justify-center items-center px-3 font-medium">{{ $suffix }}</span>
     @endif
   </span>
   {{ $slot }}
   @error($errorKey)
     <span class="text-xs text-red-700">{{ $message }}</span>
   @enderror
 </label>
