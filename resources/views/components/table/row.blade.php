@props([
    'delay' => 0,
    'offset' => 0,
])
<tr class="opacity-0 " data-aos="fade-in" data-aos-delay="{{ $delay }}" data-aos-duration="150" data-aos-offset="{{ $offset }}">
  {{ $slot }}
</tr>
