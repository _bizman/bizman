@props([
    'style' => 'btn-primary',
])
<button {{ $attributes->merge(['type' => 'submit', 'class' => $style]) }}>
  {{ $slot }}
</button>
