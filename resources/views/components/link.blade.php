<a {{ $attributes->merge(['class' => 'btn-primary']) }}>
  {{ $slot }}
</a>
