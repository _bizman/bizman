  @if (\Session::has('success'))
    <x-toast type="success">
      {!! \Session::get('success') !!}
    </x-toast>
  @elseif(\Session::has('errorMessage'))
    <x-toast type="error">
      {!! \Session::get('errorMessage') !!}
    </x-toast>
  @endif
