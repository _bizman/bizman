if (document.getElementById("suma"))
    calculate()

document.querySelectorAll('.time').forEach(function (element) {
    element.addEventListener('keyup', function () { calculate(); });
})
document.querySelectorAll('.min').forEach(function (element) {
    element.addEventListener('keyup', function () { calculate(); });
})
document.querySelectorAll('.max').forEach(function (element) {
    element.addEventListener('keyup', function () { calculate(); });
})
document.querySelectorAll('.price').forEach(function (element) {
    element.addEventListener('keyup', function () { calculate(); });
})

document.querySelectorAll('.bmin').forEach(function (element) {
    element.addEventListener('click', minu);
    function minu() {
        var id = element.dataset.element_id;
        document.getElementById("min" + id).value = document.getElementById("min" + id).value = document.getElementById("min" + id).value * 1 + 1
        calculate()
    }
})
document.querySelectorAll('.bmax').forEach(function (element) {
    element.addEventListener('click', maxu);
    function maxu() {
        var id = element.dataset.element_id;
        document.getElementById("max" + id).value = document.getElementById("max" + id).value = document.getElementById("max" + id).value * 1 + 1
        calculate()
    }
})

function calculate() {
    time = document.getElementsByClassName("time");
    price = document.getElementsByClassName("price");
    min = document.getElementsByClassName("min");
    max = document.getElementsByClassName("max");
    sum = document.getElementsByClassName("sum");
    suma = 0
    minimalna = 0
    maksymalna = 0
    for (var i = 0; i < time.length; i++) {
        if (time[i].value != '' && price[i].value != '' && min[i].value != '' && max[i].value != '') {
            time[i].value = time[i].value * 1
            price[i].value = price[i].value * 1
            min[i].value = min[i].value * 1
            max[i].value = max[i].value * 1
            sum[i].innerHTML = time[i].value * price[i].value
            suma = suma + (time[i].value * price[i].value)
            minimalna = minimalna + (time[i].value * price[i].value - time[i].value * price[i].value * (min[i].value / 100))
            maksymalna = maksymalna + (time[i].value * price[i].value + time[i].value * price[i].value * (max[i].value / 100))
        }
    }
    document.getElementById("suma").innerText = "Suma projektu: " + Math.round(suma, 2) + " zł"
    document.getElementById("optymistyczny").innerText = "Wariant optymistyczny: " + Math.round(minimalna, 2) + " zł"
    document.getElementById("pesymistyczny").innerText = "Wariant pesymistyczny: " + Math.round(maksymalna, 2) + " zł"
}

document.addEventListener('DOMContentLoaded', e => {
    calculate()
})
