export default class SettingsStatuses {


    addNewButton;
    jobList;
    rowTemplate;
    markedForDeletion;

    constructor() {
        this.addNewButton = document.querySelector('#new_status')
        this.jobList = document.querySelector('#quote_statuses_list')
        this.rowTemplate = document.querySelector('#job_status_input_template')
        this.defaultCategoryTemplate = document.querySelector('#delete_info')
        this.markedForDeletion = ['select-none', 'opacity-50', 'pointer-events-none', 'disabled']
    }

    async init() {
        console.log('SettingsStatuses');
        this.addNewListener()
        this.jobList?.querySelectorAll('.status-delete input[type="checkbox"]')?.forEach(el => {
            this.addDeleteListener(el)
        })
    }

    addNewListener() {
        this.addNewButton?.addEventListener('click', e => {
            this.disableDeletion()
            this.addNewRow()
        })
    }

    addDeleteListener(el) {
        el?.addEventListener('change', e => {
            if (this.getCheckedCount() < this.getJobTypeList().length) {
                this.toggleDelete(el)
            } else {
                el.checked = false
            }

            this.toggleAddNewRow()
        })
    }

    addNewRow() {
        const template = this.getRowTemplate();
        const newIndex = this.getJobTypeList().length;

        [...['status-state', 'status-color']].forEach(labelClass => {
            const label = template.querySelector(`label.${labelClass}`)

            if (label) {
                const input = label.querySelector(`label.${labelClass} input`)

                if (input) {
                    const segments = labelClass.split('-')
                    const name = `statuses[${newIndex}][${segments[1]}]`;
                    input.name = name
                }
            }
        })

        const removeNewRowButton = template.querySelector('.remove');

        removeNewRowButton?.addEventListener('click', e => {
            removeNewRowButton.closest('li.new-status')?.remove()
            if (this.getNewCount() == 0)
                this.enableDeletion()
        })

        this.jobList.append(template)

        const statusColorInput = template.querySelector('.status-color')

        if (statusColorInput) {
            const picker = new JSColor(statusColorInput, { backgroundColor: '#000' });
        }
    }

    getRowTemplate() {
        return this.rowTemplate.content.cloneNode(true)
    }

    getDefaultSelectionTemplate() {
        return this.defaultCategoryTemplate.content.cloneNode(true)
    }

    toggleDelete(el) {
        const rowToDelete = el.closest('li')

        rowToDelete?.classList.toggle('marked-for-deletion')
        rowToDelete?.querySelectorAll('input[type="text"]')?.forEach(el => {
            if (el.disabled)
                el.disabled = false
            else
                el.disabled = true
        })

        this.toggleDefaultCategorySelection()
    }
    disableDeletion() {
        const checkboxes = this.jobList.querySelectorAll('li input[type="checkbox"]')
        checkboxes?.forEach(box => {
            box.checked = false
            box.disabled = true
        })

    }
    enableDeletion() {
        if (this.getNewCount() == 0) {
            const checkboxes = this.jobList.querySelectorAll('li input[type="checkbox"]')
            checkboxes?.forEach(box => {
                box.disabled = false
            })
        }
    }
    toggleAddNewRow() {
        if (this.getCheckedCount() == 0) {
            this.addNewButton.disabled = false
        } else {
            this.addNewButton.disabled = true
        }
    }
    filterDefaultCategory() {
        const allowedJobs = this.jobList.querySelectorAll('li:not(.marked-for-deletion)')
        const select = document.querySelector('form .delete-info select')
        select?.querySelectorAll('option')?.forEach(el => el.remove())
        allowedJobs?.forEach(job => {
            var opt = document.createElement('option');
            opt.value = job.querySelector('.status-id').value;
            opt.innerHTML = job.querySelector('.status-state input').value;
            select.appendChild(opt);
        })
    }
    toggleDefaultCategorySelection() {
        if (this.getCheckedCount() > 0) {
            if (document.querySelector('form .delete-info') === null)
                this.addNewButton.after(this.getDefaultSelectionTemplate())

            this.filterDefaultCategory()
        } else {
            document.querySelector('form .delete-info')?.remove()
        }

    }
    getJobTypeList() {
        return this.jobList.querySelectorAll('li')
    }
    getCheckedCount() {
        return this.jobList.querySelectorAll('li input[type="checkbox"]:checked').length
    }
    getNotCheckedCount() {
        return this.jobList.querySelectorAll('li input[type="checkbox"]:not(:checked)').length
    }
    getNewCount() {
        return this.jobList.querySelectorAll('li.new-job').length
    }
    validateListCount() {
        return (this.getCheckedCount() < this.getJobTypeList().length) ? true : false;
    }

}
