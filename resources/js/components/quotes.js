document.querySelectorAll('.add-to-quote').forEach(function (element) {
    element.addEventListener('click', quote, { once: true });

    function quote() {

        //skrocenie zmiennych
        var id = element.dataset.element_id;
        var title = element.dataset.element_title;
        var content = element.dataset.element_content;
        var job_type = element.dataset.element_job;
        var work_hours = element.dataset.element_time;

        //glowny div
        var div = document.getElementById("default_item");
        var ClonedDiv = document.getElementById("item" + id);
        var divClone = div.cloneNode(true);
        divClone.classList.remove("hidden");

        //tytul
        divClone.children[0].children[0].children[0].children[0].value = title

        //content
        divClone.children[0].children[1].children[0].children[0].value = content

        //job
        divClone.children[1].children[0].children[0].children[0].value = job_type

        //time
        divClone.children[1].children[1].children[0].children[0].value = work_hours

        //guzik
        divClone.children[1].children[2].id = "item" + id + "buttonm" >
            divClone.children[1].children[2].addEventListener('click', function () {
                document.getElementById("item" + id).style.background = "rgb(243 244 246)"
                document.getElementById("item" + id + "_button").setAttribute('class', 'cursor-pointer btn-primary');
                document.getElementById("item" + id + "_button").addEventListener('click', function () { quote(); }, { once: true });
            }, { once: true });

        divClone.children[1].children[2].data - 'element_id="{{ $item->id }}"'

        ClonedDiv.children[0].children[0].setAttribute('class', 'cursor-pointer pointer-events-none btn-primary');

        //dodanie przerobionego diva na lewo
        ClonedDiv.style.background = "gray"
        document.getElementById("add_quote").appendChild(divClone)

        updateQuoteFormNames()
    }
}
)

const updateQuoteFormNames = () => {

    const pricedItems = document.querySelectorAll('.here-add-quote .priced-item-wrapper')

    pricedItems.forEach((item, index) => {

        const itemTitle = item.children[0].children[0].children[0].children[0],
            itemContent = item.children[0].children[1].children[0].children[0],
            itemJob = item.children[1].children[1].children[0].children[0],
            itemHours = item.children[1].children[1].children[0].children[0];

        itemTitle.name = `priced_items[${index}][title]`;
        itemContent.name = `priced_items[${index}][content]`;
        itemJob.name = `priced_items[${index}][job_type]`;
        itemHours.name = `priced_items[${index}][time]`;
        console.log(item.children[0].children[0].children[0].children[0].name)

    })

}
document.querySelectorAll('.delete-quote').forEach(function (element) {
    element.addEventListener('click', delete_quote, { once: true });

    function delete_quote() {
        console.log(element.dataset.element_id)
        var div = document.getElementById("item" + id);
        document.getElementById("item" + id).style.background = "rgb(243 244 246)"
        document.getElementById("item" + id + "_button").setAttribute('class', 'cursor-pointer btn-primary');
        document.getElementById("item" + id + "_button").addEventListener('click', function () { quote(); }, { once: true });
        div.style.background = "gray"
    }
})