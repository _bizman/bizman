
class Settings {
    inputClass = ['w-full', 'py-3', 'text-black', 'bg-white', 'border-none', 'outline-none', 'focus:outline-none', 'focus:border-none', 'focus:shadow-none', 'focus:ring-transparent', 'job-name'];
    labelClass = ['text-gray-500', 'text-sm', 'font-semibold', 'flex', 'flex-col', 'gap-1']
    constructor() {
        this.stateNames = document.querySelectorAll('input.quote-state')
        this.stateColors = document.querySelectorAll('input.quote-color')
        this.jobNames = document.querySelectorAll('input.job-name')
        this.jobHandles = document.querySelectorAll('input.job-handle')
        this.deleteStates = document.querySelectorAll('button.delete-quote-state')
        this.addNewQuoteState = document.querySelector('button#new_quote_status')
        this.quoteStatesWrapper = document.querySelector('#quote_states_wrapper')
        this.quoteStatesList = document.querySelector('#quote_states_list')
        this.arr = document.querySelectorAll('.setting_option').length
    }

    addDefaultState = () => {
        this.arr = this.arr - 1
        if (this.arr < 1) {
            let li = document.createElement('li')
            li.id = this.arr + 1
            li.dataset.quotestate_id = this.arr + 1
            li.classList.add("setting_option")

            let wrapper = document.createElement('div')
            wrapper.classList.add('mb-2', 'state-wrapper', 'gap-4', 'grid', 'grid-cols-[auto_180px_50px]')

            let labelInputState = document.createElement('label')
            labelInputState.classList.add(...this.labelClass)

            let inputState = document.createElement('input')
            inputState.type = 'text'
            inputState.classList.add(...this.inputClass)
            inputState.classList.add('rounded-lg')
            inputState.classList.add('quote-state')
            inputState.placeholder = 'Nazwa statusu'
            inputState.value = 'Domyślny'
            inputState.name = `states[${document.querySelectorAll('.state-wrapper').length + 1}][state]`
            inputState.setAttribute("readonly", "readonly")
            labelInputState.append(inputState)
            wrapper.append(labelInputState)

            let labelInputColor = document.createElement('label')
            labelInputColor.classList.add(...this.labelClass)

            let inputColor = document.createElement('input')
            inputColor.type = 'text'
            inputColor.classList.add(...this.inputClass)
            inputColor.classList.add('rounded-lg')
            inputColor.classList.add('quote-color')
            inputColor.placeholder = 'Kolor'
            inputColor.value = "#FFFFFF"
            inputColor.name = `states[${document.querySelectorAll('.state-wrapper').length + 1}][color]`
            inputState.setAttribute("readonly", "readonly")
            labelInputColor.append(inputColor)
            wrapper.append(labelInputColor)

            li.appendChild(wrapper)
            this.arr = this.arr + 1
            this.quoteStatesList.append(li)

            document.getElementById("settings_data").submit()
        }

        if (this.arr > 0) {
            this.removeDefaultState()
        }
    }
    removeDefaultState = () => {
        if (!document.getElementById("1").type == "hidden") {
            if (document.getElementById("1").children[0].children[0].children[0].children[0].value == 'Domyślny') {
                document.getElementById("1").parentNode.removeChild(document.getElementById("1"))
            }
        }
    }

    quoteStateDeleteTemplate = (id) => {
        let tmp = document.createElement("input");
        tmp.type = 'hidden'
        tmp.name = 'delete[states][]'
        tmp.value = id
        return tmp;
    }
    quoteStateAddTemplate = () => {
        let li = document.createElement('li')
        li.id = this.arr + 1
        li.dataset.quotestate_id = this.arr + 1
        li.classList.add("setting_option")

        let wrapper = document.createElement('div')
        wrapper.classList.add('mb-2', 'state-wrapper', 'gap-4', 'grid', 'grid-cols-[auto_180px_50px]')

        let labelInputState = document.createElement('label')
        labelInputState.classList.add(...this.labelClass)

        let inputState = document.createElement('input')
        inputState.type = 'text'
        inputState.classList.add(...this.inputClass)
        inputState.classList.add('rounded-lg')
        inputState.classList.add('quote-state')
        inputState.placeholder = 'Nazwa statusu'
        inputState.name = `states[${document.querySelectorAll('.state-wrapper').length + 1}][state]`
        labelInputState.append(inputState)
        wrapper.append(labelInputState)

        let labelInputColor = document.createElement('label')
        labelInputColor.classList.add(...this.labelClass)

        let inputColor = document.createElement('input')
        inputColor.type = 'text'
        inputColor.classList.add(...this.inputClass)
        inputColor.classList.add('rounded-lg')
        inputColor.classList.add('quote-color')
        inputColor.placeholder = 'Kolor'
        var opts = { backgroundColor: '#333' };
        var picker = new JSColor(inputColor, opts);
        inputColor.value = "#FFFFFF"
        inputColor.name = `states[${document.querySelectorAll('.state-wrapper').length + 1}][color]`
        labelInputColor.append(inputColor)
        wrapper.append(labelInputColor)

        let deleteButton = document.createElement('button')
        deleteButton.type = 'button'
        deleteButton.classList.add('h-full', 'px-3', 'mx-auto', 'text-sm', 'reverse', 'delete-quote-stat', 'btn-delete')
        deleteButton.innerHTML = 'Usuń'
        wrapper.append(deleteButton)
        deleteButton.addEventListener('click', e => {
            this.handleDeleteQuoteState(deleteButton)
            this.removeDefaultState()
        })

        li.appendChild(wrapper)
        this.arr = this.arr + 1
        this.quoteStatesList.append(li)
        this.removeDefaultState()
    }


    handleDeleteQuoteState = (el, existing = true) => {
        if (existing) {
            let id = el.dataset.quotestate_id;
            let removeTemplate = this.quoteStateDeleteTemplate(id);
            el.parentNode.after(removeTemplate)
            this.addDefaultState()
        }

        el.parentNode.remove()
    }

    addListeners = () => {
        this.deleteStates.forEach(el => {
            el.addEventListener('click', e => {
                this.handleDeleteQuoteState(el)
            })
        })

        this.addNewQuoteState.addEventListener('click', e => {
            this.quoteStateAddTemplate()
            this.arr += 1
        })
    }

    init = () => {
        if (this.deleteStates.length) {
            this.addListeners()
        }
        if (document.querySelectorAll('.setting_option').length == 1 || document.querySelectorAll('.setting_option')) {
            console.log(document.querySelectorAll('.setting_option').length)
            if (document.getElementById("1").children[0].children[0].children[0].children[0].value == 'Domyślny') {
                document.getElementById("1").parentNode.removeChild(document.getElementById("1"))
            }
        }
        if (document.querySelectorAll('.setting_option').length > 1) {
            if (document.getElementById("1").children[0].children[0].children[0].children[0].value == 'Domyślny') {
                document.getElementById("1").children[0].children[0].children[0].children[0].disabled = true
                document.getElementById("1").children[0].children[1].children[0].children[0].disabled = true
                document.getElementById("1").children[0].removeChild(document.getElementById("1").children[0].children[3])
            }
        }
    }
}



export default Settings;
