import tinymce from "tinymce";
import theme from 'tinymce/themes/silver/theme';
import model from 'tinymce/models/dom/model';
import icons from 'tinymce/icons/default/icons';


export default class PricedItem {

    itemContent;

    constructor() {
    }

    async init() {
        this.addTinyMceEditor()
    }

    addTinyMceEditor() {
        tinymce.init({
            selector: 'textarea',
            language: 'pl'
        });
    }
}
