export default class Calculator {

    calculator;
    calculatorSummary;
    totalNegative;
    total;
    toalPositive;

    constructor() {
        this.calculator = document.querySelector('.calculator')
        this.calculatorSummary = document.querySelector('.calculator-summary')
    }

    setTotalValues() {
        this.total = 0;
        this.totalPositive = 0;
        this.totalNegative = 0;
    }

    async init() {
        this.addListeners()
        this.limitNumberInput()
        this.recalculate()
    }

    updateTotals() {
        this.calculatorSummary.querySelector('.total-negative').value = Number(this.totalNegative).toFixed(2);
        this.calculatorSummary.querySelector('.total').value = Number(this.total).toFixed(2);
        this.calculatorSummary.querySelector('.total-positive').value = Number(this.totalPositive).toFixed(2);
    }

    addListeners() {
        this.calculator?.querySelectorAll('input[type="number"]:not(:disabled)')?.forEach(el => {
            el.addEventListener('input', e => {
                this.recalculate()
                this.disablePdfButton()
                this.enableSaveButton()
            })
        })
    }

    recalculate() {
        this.setTotalValues()

        const rows = this.calculator.querySelectorAll('.job-type-wrapper')

        rows?.forEach(row => {
            const hours = Number(row.querySelector('.hours').value),
                perHour = Number(row.querySelector('.per-hour').value),
                negative = Number(row.querySelector('.negative').value),
                positive = Number(row.querySelector('.positive').value),
                rowTotal = row.querySelector('.row-total');

            const total = hours * perHour;
            const negativeTotal = total + (total * (negative / 100))
            const positiveTotal = total + (total * (positive / 100))

            rowTotal.value = Number(total).toFixed(2);
            this.total = total + Number(this.total);
            this.totalPositive = Number(positiveTotal) + Number(this.totalPositive);
            this.totalNegative = Number(negativeTotal) + Number(this.totalNegative);
        })

        this.updateTotals()
    }

    disablePdfButton() {
        document.querySelector('.generate-pdf')?.classList.add('disabled')
    }

    enableSaveButton() {
        document.querySelector('.save-button')?.classList.remove('hidden')
    }

    limitNumberInput() {
        this.calculator?.querySelectorAll('input[data-min]')?.forEach(el => {
            el.addEventListener('input', e => {

                const min = Number(el.dataset.min);
                const max = Number(el.dataset.max);
                const negative = max < min;

                if (negative) {
                    if (el.value > min) {
                        el.value = min;
                    } else if (el.value <= max) {
                        el.value = max;
                    }
                } else {
                    if (el.value >= max) {
                        el.value = max;
                    } else if (el.value <= min) {
                        el.value = min;
                    }
                }

            })
        })
    }
}
