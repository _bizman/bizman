import tinymce from "tinymce";
import theme from 'tinymce/themes/silver/theme';
import model from 'tinymce/models/dom/model';
import icons from 'tinymce/icons/default/icons';

export default class EditQuote {

    searchInput;
    searchList;
    copyTempale;
    quoteList;
    addNewButton;

    constructor() {
        this.searchInput = document.querySelector('.quote-search')
        this.searchList = document.querySelector('.priced-list')
        this.quoteList = document.querySelector('.quote-list')
        this.copyTempale = document.querySelector('#item_template')
        this.addNewButton = document.querySelector('.add-new-item')
    }

    async init() {
        this.addListener()
        this.initTiny()
        this.handleAdded()
    }

    addListener() {
        this.searchInput?.addEventListener('input', e => {
            this.filterPricedItems(this.searchInput.value)
        })
        this.searchList.querySelectorAll('.add-to-quote')?.forEach(el => {
            el.addEventListener('click', e => {
                this.copyPricedItem(el.closest('li'))
            })
        })
        this.addNewButton?.addEventListener('click', e => {
            this.addNewPricedItem()
        })
    }

    filterPricedItems(string) {
        const searchTerm = String(string).toLowerCase();
        const elements = this.searchList.querySelectorAll('li');

        if (searchTerm.length >= 1) {
            elements?.forEach(el => {
                const searchData = JSON.parse(el.dataset.search);

                if (searchData.title.indexOf(searchTerm) === -1 && searchData.content.indexOf(searchTerm) === -1) {
                    el.classList.add('hidden')
                } else {
                    el.classList.remove('hidden')
                }
            })
        } else {
            elements?.forEach(el => {
                el.classList.remove('hidden')
            })
        }

    }

    addPricedItemDisable(item) {
        item?.classList.add(...['opacity-50', 'pointer-events-none'])
    }

    removePricedItemDisable(item) {
        item?.classList.remove(...['opacity-50', 'pointer-events-none'])
    }

    getTemplate() {
        return this.copyTempale.content.cloneNode(true)
    }

    copyPricedItem(item) {
        this.addPricedItemDisable(item)

        const copyData = JSON.parse(item.dataset.copy);
        const template = this.getTemplate();
        const itemsCount = this.quoteList.querySelectorAll('li').length;

        template.querySelector('.title').value = copyData.title;
        template.querySelector('.content').value = copyData.content;
        template.querySelector('.job-type-id').value = copyData.job_type_id;
        template.querySelector('.work-hours').value = copyData.work_hours;

        template.querySelector('li').dataset.copyid = item.id;

        template.querySelector('.remove-from-quote')?.addEventListener('click', e => {
            this.quoteList.querySelector(`li[data-copyid="${item.id}"]`)?.remove()
            this.removePricedItemDisable(this.searchList.querySelector(`#${item.id}`))
        })

        this.quoteList.append(template)
        this.fixNames()
        this.initTiny()
    }

    addNewPricedItem() {
        const template = this.getTemplate();

        template.querySelector('.remove-from-quote')?.addEventListener('click', e => {
            e.target.closest('li').remove()
        })

        this.quoteList.append(template)
        this.fixNames()
        this.initTiny()
    }

    initTiny() {
        tinymce.init({
            selector: '.quote-list textarea',
            language: 'pl'
        });
    }

    handleAdded() {
        const items = this.quoteList.querySelectorAll('li');

        items?.forEach(item => {
            if (item.classList.contains('priced')) {
                item.querySelector('.remove-from-quote')?.addEventListener('click', e => {
                    item.classList.add(...['opacity-50', 'pointer-events-none'])
                    item.querySelector('.to-be-removed').checked = true;
                    this.disablePricedList()
                })
            }
        })
    }

    disablePricedList() {
        this.searchList.classList.add(...['opacity-50', 'pointer-events-none'])
    }

    enablePricedList() {
        this.searchList.classList.remove(...['opacity-50', 'pointer-events-none'])
    }

    fixNames() {
        const items = this.quoteList.querySelectorAll('li');

        items?.forEach((el, index) => {
            el.querySelector('.title').name = `quote_items[${index}][title]`;
            el.querySelector('.content').name = `quote_items[${index}][content]`;
            el.querySelector('.job-type-id').name = `quote_items[${index}][job_type_id]`;
            el.querySelector('.work-hours').name = `quote_items[${index}][work_hours]`;
            el.querySelector('.add-to-priced').name = `quote_items[${index}][add_to_priced]`;
        });
    }
}
