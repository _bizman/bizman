import Alpine from 'alpinejs';
import SettingsCategories from './components/settings/categories';
import SettingsStatuses from './components/settings/statuses';
import PricedItem from './components/priceditem/edit';
import Calculator from './components/quote/calculator';
import CreateQuote from './components/quote/create';
import EditQuote from './components/quote/edit';

import AOS from 'aos';
import 'aos/dist/aos.css';

window.Alpine = Alpine;
Alpine.start();
AOS.init();

const routes = [

    [/(\/)\buser\b(\/)\bsettings\b(\/)\bcategories\b/g, [
        SettingsCategories
    ]],

    [/(\/)\buser\b(\/)\bsettings\b(\/)\bstatuses\b/g, [
        SettingsStatuses
    ]],

    [/(\/)\bpriceditems\b(\/)\bcreate\b/g, [
        PricedItem
    ]],

    [/(\/)\bpriceditems\b(\/)(\d)+(\/)\bedit\b/g, [
        PricedItem
    ]],

    [/(\/)\bquotes\b(\/)(\d)+$/g, [
        Calculator
    ]],

    [/(\/)\bquotes\b(\/)\bcreate\b/g, [
        CreateQuote
    ]],

    [/(\/)\bquotes\b(\/)(\d)+(\/)\bedit\b/g, [
        EditQuote
    ]],
];

document.addEventListener('DOMContentLoaded', e => {

    routes.forEach(async (route, index) => {
        if (route[0].test(window.location.pathname)) {
            route[1]?.forEach(async fn => {
                const instance = new fn();
                if (instance.init) await instance.init();
            })
        }
    });
})

